# Flight Controllers
This document attempts to document each of the club owned flight controllers, their primary/current use, any known limitations, and potential bugs/mitigations.

## TODO: Holybro Pixhawk 6X

## TODO: Holybro Pixhawk 5X

## Holybro Pixhawk 4
Acquired: Purchased as part of the Holybro S500 kit that became Micron v3, the club's first semi-success

Manufacturer: Holybro

Specs:
* FMUv5
* F7 + IO board
* 8IO + 8FMU (theoretical) outputs

Current usage: Currently used as the flight controller on the UGV. When not on the UGV, it is used for developmental testing.

Benefits:
* Plastic frame = lighter
* One of our smallest form factor autopilots

Shortcomings:
* It has been involved in multiple high-altitude crashes (with Micron and various prototype planes). It seems to be working fine but something to keep in mind.
* Currently missing 2 of the 4 torx case screws.
* No internal servo rail, instead external servo rail connected via JST-GH. This has been a source of annoyance and loose connections so be warned.
* As with other FMUv5, the 8 FMU ports are not always all usable for everything. For instance, only 4 are available for DSHOT, meaning an octocopter cannot be used with DSHOT.
* Plastic frame = less stronk
* Internal vibration dampening is not as good as others, recommended use external vibration dampening

Notes: N/A

## CUAV V5+
Acquired: Received as part of a giveaway when CUAV was releasing the board. Became the first flight controller used on Macron, and served well for a year.

Manufacturer: CUAV

Specs:
* FMUv5
* F7 + IO boaard
* 8IO + 6FMU (theoretical) outputs

Current usage: Currently used for developmental testing.

Benefits:
* Metal frame = stronk
* Good internal vibration dampening
* Good amount of peripherals
* Removable FMU core for custom carrier board
* Onboard servo rail

Shortcomings:
* ~~Due to a misread of documentation regarding the power input and the Matek PDB not including a voltage divider, the ADC was burned out.~~ This has been repaired by CUAV.
* As with other FMUv5, the 8 FMU ports are not always all usable for everything. For instance, only 4 are available for DSHOT, meaning an octocopter cannot be used with DSHOT. Additionally this only has 6 FMU outputs anyway.
* Only having 6 FMU outputs is a minor limitation when using 8 IO outputs for octocopter ESCs. This has been at least partly mitigated by using UAVCAN escs.

Notes: N/A

## CUAV Nora
Acquired: Received as a sponsorship from CUAV when releasing the board.

Manufacturer: CUAV

Specs:
* FMUv6x (ish)
* H7, no IO board
* 14FMU outputs

Benefits:
* Metal frame = stronk
* Not very large form factor

Current usage: Currently used on Macron.

Benefits:
* Metal frame = stronk
* Good internal vibration dampening
* More UARTs and FMU outputs than other FCs
* H7 = speed (especially for upcoming multi EKF)
* Not as large as V5+
* Onboard servo rail

Shortcomings:
* Smaller form factor means less peripherals (such as no SBUS out)
* Not modular
* Documentation is not great
* As of time of writing, PX4 doesn't properly support all 14FMU outputs. However this should change.

Notes:
Unlike other FCs, SBUS input is on the servo rail, not via a JST conenctor.

## Pixhawk 4 Mini
Acquired: David Sidrane donated it to Kalyan.

Manufacturer: Holybro

Specs:
* FMUv5
* F7 + IO board
* 8IO + 8FMU (theoretical) outputs

Current usage: Not currently in use.

Benefits:
* Metal frame = stronk

Shortcomings:
* As with other FMUv5, the 8 FMU ports are not always all usable for everything. For instance, only 4 are available for DSHOT, meaning an octocopter cannot be used with DSHOT.
* Internal vibration dampening is not as good as others, recommended use external vibration dampening
* Something seems to be wrong with the barometer.

Notes:
Currently does not work for full flight due to the barometer being missing (hardware failure??). Hopefully this can be fixed.

## NXP RDDRONE-HGDRONEK66
Acquired: Acquired as part of the NXP RDDRONE drone kit donated by NXP for participation in Hovergames.

Manufacturer: NXP

Specs:
* FMUK66 (Not strictly part of any standard)
* Kinetis K66 CPU, no IO
* 6FMU outputs

Current usage: Hovergames drone.

Benefits:
* Plastic frame = lightweight
* T1 Ethernet capability (not tested)

Shortcomings:
* Not an official Pixhawk standard
* No IO board
* Only 6 FMU outputs (come on, can't even use it on an octocopter)
* Connectors aren't very standard, especially for RC

## Unknown Model F3 Betaflight
Acquired: Purchased early in club for Micron v1

Manufacturer: Unknown

Specs:
* Unknown (not Pixhawk compatible)
* F3

Current usage: Currently being used by Kai on his personal 3d printed drone.

Benefits:
* Very small form factor
* Runs betaflight

Shortcomings:
* Not up to date since F3 ran out of flash storage for new betaflight versions
* Not nearly powerful enough to run PX4/Ardupilot
* Not a lot of peripherals
* some of the pads are in bad shape

Notes: N/A
