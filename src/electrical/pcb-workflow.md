# PCB Design Workflow

This article goes over the entire workflow of designing a circuit board to use on an AmadorUAVs vehicle or ground device. It includes considerations and checklists to go from an initial project idea to a working board. This article assumes the reader is already versed in PCB design fundamentals and knows how to use KiCAD.

Along with this article, refer to the various "chip selection guides" and KiCAD reference schematics and layouts.

Two key themes are repeated here:
1. **Hardware is not software; do not treat it as such**. For a company, software tends to be more expensive to develop due to labor costs. However, for our club work, hardware is usually the harder one and must be treated differently from software workflows. Hardware cannot be edited after the fact as easily as software. Hardware cannot be easily tested without manufacturing it. Hardware has long lead times which must be considered. **And hardware costs money**.
2. **Do not rush**. A rushed design leads to forgotten elements that may be crucial. A rushed schematic leads to silly errors. A rushed layout leads to bad electrical characteristics (crosstalk, power dissipation, and EMI), difficulty in modifying the layout (good layouts leave space to improve them later), and horrible aesthetics. Even if you catch these errors in the final review phase (good, that's much better than not catching them) it wastes lots of time to go back and change it later (especially if you have to rework the board considerably to encorporate the changes, as is often the case).

## Project Outline
The first and most important part of designing a board is to NOT OPEN UP KICAD RIGHT AWAY. Instead, open up a doc or spreadsheet and write down the answer to the questions listed below.

Start by planning out your design. This consists of 4 stages:
1. *What is your design supposed to do, really?* Plan out the exact use case for your board - what it's supposed to do, and what it's *not* supposed to do (out of scope). This may sound obvious, but throwing together a board before you know what its supposed to do. If your board is compute-intensive, what resources does it need? What is/are the operating voltage(s)? How much current will it consume?

2. What interfaces does your board need? A board needs to communicate with the rest of the world. Open up a spreadsheet and make a list of all possible interfaces you may need with your board. These include, but are not limited to:
    * Battery power (high voltage)
    * Logic power (low voltage, 5V or 3V3)
    * Power out (only for power delivery boards)
    * Programming and debug: SWD or JTAG, UART flashing, USB flashing
    * Debug UART (possibly two way or one way)
    * External UART/telemetry interfaces
    * External I2C or SPI for add-on sensors
    * CAN (FD?) for bus communication
    * Ethernet for bus communication
    * PWM interfaces for driving power electronics or servos/ESCs
    * Mezannine connections to other boards
    * GPIO for auxiliary control or anything you haven't thought of

    That last one is important. Adding extra GPIO on your board will increase its shelf life if you think of anything later on that you need to control. However, don't go overboard - exposing 10 or 20 IO pins of a chip out just for "future proofing" will be counterproductive, wasting board space and complicating routing.

    Try to think of all the interfaces you *need or want*, not all the interfaces *you can provide* with the hardware you're thinking of providing. Designs should fullfill requirements, not expose what's convenient. If it would be nice (but isn't required) to have something that your chip can't provide, write it down explicitly as a feature that was considered but isn't being provided by this revision of the board, rather than ommiting it from the document.

3. What's your budget? Setting a budget will determine a lot of things.
    * What performance do you need? What chips provide that performance and how much do they cost?
    * What types of connectors will you use?
    * How many interfaces from part 2 will you provide or ommit to save money?
    * What PCB quality do you require? What manufacturer do you plan to use as a result?
    * How many layers? 4 layer boards are generally prefered since they are still relatively cheap, provide much cleaner routing (no routing ground and power with signals), and improve ground paths and power characteristics. However, you may not need 4 (and may want to cut costs down on a large board) or may need more (for BGA routing or high-density interconnect).
    * Will you be soldering by hand, reflowing manually, or getting the board manufactured professionally?
    * Do you need double-sided assembly?

4. Plan out what types of components you need will need. This includes the microcontroller(s), other large peripherals and transceivers, power delivery, and connectors. You don't need to choose every single small component at this time, but all important components should be identified. See (insert article here) to learn how to choose a microcontroller for your project and for official AmadorUAVs recommendations. Based on the operating voltage and current, choose the required linear or switching regulator.

Before going on with the next step, have a peer (and additionally your team lead) review your document and spreadsheet to make sure nothing was missed. Peer review here is very important.

## Schematic Design

Now, you can open up KiCAD and start designing your circuit. Start by adding all your connectors (by referencing and copying your spreadsheet from the previous step), followed by your microcontroller and other important/large components, and then the smaller components and passives. There isn't too much to say about schematic design itself. Instead, this section includes a list of tips/considerations as well as checklists for common design components and peripherals.

### Tips & Considerations

### Peripheral Checklists

#### Programming

There are a few options for programming:
1. Use SWD on ARM mircontrollers. This is simple and reliable but requires an external programmer (which you should probably buy anyway). There is also JTAG programming on non-ARM microcontrollers.
2. Use the Arduino bootloader. This is probably the simplest solution for AVR boards but can also be used for other types (not recommended, however).
3. Use ICSP for AVR MCUs.
4. Use the DFU bootloader on some STM32 microcontrollers.
5. Use the USB UF2 bootloader on RP2040.
6. Use the UART bootloader on ESP32 microcontrollers.

#### SWD Debug
Try to include a debug connector even if you're using an alternate programming method. SWD applies to ARM MCUs only.

Pins:
* SWDIO (bidirectional data pin)
* SWCLK (clock pin)

#### JTAG Debug
Try to include a debug connector even if you're using an alternate programming method. SWD applies to non ARM MCUs only.

Pins:
* TDI (test data in)
* TDO (test data out)
* TCK (test clock)
* TMS (test mode select)

#### UART

UART has two required pins:
* TX (data transmit)
* RX (data receive)

Remember that for UART communication from one device to another, the TX of one device goes to the RX pin of the other. This makes sense as the data goes out from one device and into the other. Try to think about it logically instead of memorizing "swap the pins" to prevent errors. Check and double check that you've wired these up correctly.

UART also has optional flow control pins:
* RTS (request to send)
* CTS (clear to send)

These are rarely necessary in practice these days. However, you may want to add them if you can afford to, and they are required on certain interface standards like the Pixhawk DS-009 UART telemetry pinout. They are also useful for RS-485, etc.

#### SPI

SPI has 4 required pins:
* PICO/MOSI (controller -> peripheral data)
* POCI/MISO (peripheral -> controller data)
* SCK (serial clock)
* nCS (active-low chip select)

Remember that the chip select is active-low. Usually this will just be wired to a GPIO. However, add a DNP pulldown resistor to enable the chip if GPIO doesn't work, or for testing. Also consider that you may need multiple chip selects.

#### I2C

I2C has 2 required pins:
* SCL (serial clock)
* SDA (bidirectional serial data)

Because I2C is driven as open-drain, these pins MUST be pulled up on a custom board. Pull up to 3V3 through 4.7K resistors unless otherwise specified. The Pixhawk standards recommend 1.5K so use that for DS-009 compatible interfaces.

#### CAN

CAN has two pins from the controller to the transceiver:
* TX (data transmit)
* RX (data receive)
And two bus lines:
* CAN_H (differential data high)
* CAN_L (differential data low)

If your CAN transceiver has a "silent" mode, make sure that is pulled active by default, so the transceiver is silent until it is pulled down by the microcontroller to enable. This makes sure a malfunctioning board stays silent on the bus. Also consider adding a DNP pulldown resistor on this line in case your silent functionality doesn't work.
