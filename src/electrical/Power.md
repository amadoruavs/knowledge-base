# Power Distribution
This document describes Macron's power distribution setup.

## Voltage & Current Consumption
Macron runs on 12S (44.4v). TODO elaborate.

## Battery Setup
At time of writing, the club owns 8 sets of batteries, which were received along with Macron. The batteries come from different manufacturers but they are all 6S (22.2V) 10,000mAh LiPos. Macron runs on 12S, so we use a combination of series and parallel harnesses to provide power:
Two batteries are wired in series to increase voltage to 44.4V. This is sufficient to fly Macron. However, for increased flight time, two sets of these are used - connected in parallel for a total of 44.4V * 20,000mAh.

## Central Distribution
Currently, power distribution on Macron is quite simple. We use a Matek X-Class PDB as it is one of few PDBs that can support the high current draw of our octocopter. Power input comes from the batteries and is distributed to the ESCs and peripherals.

## ESCs
There are 4 ESC pads on the Matek PDB as it is designed for quad copters. However, this does not matter in practice as we just solder to both sides of the pad, allowing us to power all 8 ESCs. Two cables for each ESC (+ and -) run down each arm, and are terminated by an XT60 connector. Benefits of this are:
* Convenience of assembly/disassembly because of XT60
* No reverse polarity accidents
* Two wires running down the arms instead of 3 motor phase wires decreases wire cost

## Flight Controller
Note: This is under review/improvement.
An additional XT60 connector is connected to one of the pads and is used to power the FC. We simply plug the XT60 into the XT60 of the FC's own power brick, which also provides voltage sensing. Since this is wired in parallel and therefore the majority of power is not going through it, current sensing is not accurate here. Instead, the current pad from the Matek pdb is spliced into the FC power input.

## Ubiquiti Bullet
The Bullet is powered by the 12V regulator on the Matek PDB. This is wired to a barrel jack which is then plugged into a PoE injector which connects the Jetson's ethernet port to the Bullet.

## Jetson Nano
The Jetson has 3 valid sources of power:
1. Micro USB (5V 2A)
2. Barrel Jack (5V 4A)
3. GPIO (5V 3A) (x2)
The Jetson is powered by a 5V regulator on the Matek PDB. Currently the GPIO rail is used to power the Jetson as it is the most convenient option that provides enough power. Micro USB is insufficient as the Jetson has been observed to shut down as a result of insufficient power for both the GPU and peripheral components (such as the USB camera). Two GPIO pins can be used in theory for 6A total, but this is yet to be tested.

## Servos
Auxiliary servos are connected to the FC. However, as the FC servo rail does not provide power, another 5V regulator is connected to the rail.

## Camera
At time of writing, the only camera in use is the Imaging Source USB camera which is powered via USB.
