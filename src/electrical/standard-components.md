# AmadorUAVs Electrical Standards
Updated May 2022

These are the standard components in catalog for the AmadorUAVs Electromechanical team. Electrical components for use in KiCad
can be found in [Standard Libraries](https://gitlab.com/amadoruavs/electrical/standard-libs).

## For onboard electrical systems
AmadorUAVs standardizes on specific electrical components for system reliablity, user safety, and ease of manufacturing.

**NOTE: Components in BOLD are considered preferred. Use if possible for the listed use case**

### Connectors

General notes: 
- High power connectors must be capable of withstanding the current required by the application. 
- Locking connectors are preferred in all situations for onboard electronics. High vibrations can be experienced.
- Use DIN rail power distribution for large-gauge wire.
- **Use mechanical wire connections instead of solder connections when possible (Ferrules, etc).** 


| Component | Use | Link | Image |
| --------- | --- | ---- | ----- |
| **AMASS XT90S-F** | Antispark XT90 connector (Only for female). For battery-side connection. **NOTE: WIRE BOOT MUST BE USED AT ALL TIMES** | [TME Link](https://www.tme.com/us/en-us/details/xt90s-f/dc-power-connectors/amass/) | ![XT90S-F](./images/connectors/XT90S.png) |
| **AMASS XT90H-M** | XT90 male connector. For drone PDB/power delivery side connector. **NOTE: WIRE BOOT MUST BE USED AT ALL TIMES** | [TME Link](https://www.tme.com/us/en-us/details/xt90h-m/dc-power-connectors/amass/) | ![XT90H-M](./images/connectors/XT90H-M.png) |
| **AMASS MT60** | 3-pin 60 amp motor connectors. For brushless motor connection to ESC. **NOTE: WIRE BOOT MUST BE USED AT ALL TIMES** | [TME Link for MALE](https://www.tme.com/us/en-us/details/mt60-m/dc-power-connectors/amass/)<br><br>[TME Link for FEMALE](https://www.tme.com/us/en-us/details/mt60-f/dc-power-connectors/amass/) | ![MT60](./images/connectors/MT60.png) |
| **Molex MicroFit 3.0** | Multipin locking auxillary connectors. Can be used for sensors, small electronics, power distribution, etc. | [Molex Official Link](https://www.molex.com/molex/products/family/microfit_30)<br><br>[DigiKey Product Highlight](https://www.digikey.com/en/product-highlight/m/molex-connector/micro-fit-3-interconnect-system) | ![Microfit 3.0](./images/connectors/microfit.png) |
| **XT60H Connectors** ||||
| XT90H-F| Non-antispark XT90 female connector. Not preferred. |||
| XT30 Connectors ||||
| Bullet Connectors ||||
| Dupont Connectors ||||

### Regulators
As of November 2022, components have not been chosen due to stock issues. However, the following standards are in place for component research.

- **Mean Well Open Frame [NID Series](https://www.meanwell.com/webapp/product/search.aspx?prod=NID100)**
    - For critical electronics and high power regulation

### Motors
The following approved brands may be used in production:
- [T Motor](https://uav-en.tmotor.com/)
- [KDE](https://www.kdedirect.com/) Far too expensive.

In prototyping:
- [T Motor](https://uav-en.tmotor.com/)


## For custom circuit development use on PCBs

### Connectors
- U254-051T-4BH83-F1S: Micro-USB Connector
- Terminal Block: Phoenix 5.08mm (Standard Library)
- 2.54mm Dupont Pin Header

### Regulators
- BL9161-33BARN/HX6211A332MR: 3.3V LDO Regulator, standard for MCU power
- LDK120M18R: 5V LDO Regulator

### Switches
- TS_1088R: SPST Push-button Switch

### Microcontrollers
- STM32G491CET6: 48-pin LQFP (standard package, custom schematic symbol), CAN-FD enabled, high compute power
- STM32G0B1KE: 32-pin LQFP (standard package, custom schematic symbol), CAN-FD enabled, medium compute power
- STM32G030F6P6: 20-TSSOP (standard package, custom symbol), no CAN, low power, good for small applications
- ESP32-WROOM-32(U/D) module: Wi-Fi enabled high-power/high-performance MCU (Standard Library)
- RP2040: 56-pin QFN (standard package, custom symbol), very high compute power, high supply, use where G030 not applicable and CAN not required
- ATSAMD21E17D-AU: 32-pin TQFP, low compute (Standard Library)
- ATSAMD21G17D-AU: 48-pin TQFP, low compute (Standard Library)
- Atmega328P (Arduino Uno, Kduino) - **PROTOTYPING ONLY**

### Transceivers
- MAX3232: 3V3 RS-232 Transceiver, SOIC-16
- MAX3485: 3V3 RS-485 Differential Transceiver, SOIC-8
- MCP2544FD-H/SN: CAN-FD 8mbps Transceiver, TJA1051 Pin-Compatible, SOIC-8

### Sensors
- ICM20789: Standard 7-axis IMU+baro, 24-QFN
- ICM20689: Standard 6-axis IMU, 24-QFN
- MMC5633NJL: Standard 3-axis Magnetometer, 4-WLP-0.86
