<div id="frontmatter" style="display: none">
title: "Website"
</div>


# Website
The following describes the setup/configuration of the AmadorUAVs website and DNS configuration for https://amadoruavs.com

## Google Workspace
A free version of GSuite/Google Workspace is used to manage the club email. This provides us with officers@amadoruavs.com. In the future additional email accounts may be created but currently a good solution that is free of cost has not been found (adding additional accounts to the GSuite plan incurs cost). To access the account email, sign in to officers@amadoruavs.com with the standard credentials using GMail.

## DNS
Cloudflare is the DNS provider for amadoruavs.com. The credentials for logging in are the standard officers email credentials at cloudflare.com. Cloudflare is also used as a cache/proxy for the static site.

### Configuration:
* `@` and `www` point to the main website. At time of writing this is a static site hosted on netlify, so these are just CNAME records pointing to amadoruavs.netlify.app
* `server.amadoruavs.com` points to the personal home IP of the host of the AmadorUAVs server and related infrastructure (see Server.md). At time of writing this is Kalyan.
* There are various CNAME records pointing to ghs.googlehosted.com which manages the Google Workspace stuff.
* There are 2 CNAME records which setup the DKIM records for mailchip which the business team uses to manage the newsletter.

## Hugo
The website is a static site generated using the [Hugo](https://gohugo.io) SSG. TODO.

## Netlify CMS
Netlify CMS is used to provide clean integration between the Hugo site built by the software division and the rest of the team who needs an easy non-code way to write blog and news articles. TODO

## Netlify Hosting
Netlify is used to host the site. TODO
