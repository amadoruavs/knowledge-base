<div id="frontmatter" style="display: none">
title: "Server"
</div>

# AmadorUAVs Server

**As of December 2021, the server is no longer maintained.**
**This article is archived for reference.**

The software division operates a server that can be used for shared work, computationally intensive or long-running jobs, storage space, etc. Additionally, other devices are usually accessible as well:
* Jetson Nano
* Hovergames Navq

## Running Services
* Main server:
    - Alpine Linux
    - SSH (port 22001)
    - Docker & Podman
    - Gazebo gzserver (TODO)
    - Interop Server (port 8000)

## Connecting
At time of writing, the server is hosted at Kalyan's house. This can be reached by the url `server.amadoruavs.com`, or if that doesn't work, with the IP `24.4.142.86`.
The server exposes an ssh daemon that is forwarded on port 22001. To connect to it:
The credentials are the *standard software division credentials*.
```
$ ssh amadoruavs@server.amadoruavs.com -p 22001
```

## Interop Server
An instance of the interop server is running at port 8000. This is useful for testing mission code in a reproducible way (and it's convenient since you don't have to run your own).
The django web admin can be accessed at `http://server.amadoruavs.com:8000`

## Jetson Nano
The Jetson Nano is either on the drone or next to the server (see Networking.md). Either way, it is accessible over SSH:
```
$ ssh amadoruavs@server.amadoruavs.com -p 22002
```

## Navq
Similary, the navq is available to SSH on port 22003:
```
$ ssh amadoruavs@server.amadoruavs.com -p 22003
```
