# Sponsor Info
General guide on sponsorships, partnerships, and donations.

### Stuff that wont work. Do not try or waste time on:
* Door to door with small local businesses
    - As a high school engineering club, we have absolutely nothing important or useful to offer to small retail businesses. Come up with a real solution to that and try again
* Texting companies through their Instagram pages

### Stuff that will work, LOW SUCCESS RATE
* Dry emailing companies
* Remember that grants are difficult. 
    - In addition to adhering to application guidelines, you must have an extremely strong objective statement
    - Often times, an exact purchase sheet of what you need is required
    - More importantly you need to be able to report on the results of your project in much detail

### Stuff that has a higher success rate
* Contact related companies by phone. Talk to a person.
* Email a company through a connection 
