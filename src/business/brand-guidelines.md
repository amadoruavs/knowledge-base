# Brand Guidelines 
Updated November 2022

## General

### Brand Statement
> AmadorUAVs is a club at Amador Valley High School in Pleasanton, CA. This club was formed in 2018 with the intent of developing Unmanned Aerial Vehicles (UAVs).

### Brand Tone
**Informal**

AmadorUAVs' tone is generally informal in public marketing through social media - it is friendly, appreciative, and engaging but avoids jargon and opinionated speech. 

**Formal**

AmadorUAVs utilizes a formal tone for external communications, sponsor relations, and event planning.

## Publication Specifications
**Profile picture dimensions:**

150 x 150

**Instagram post dimensions:**

1080 x 1080

**Instagram story dimensions:**

1080 x 1920

**Video:**

- At least 1920px x 1080px
- Maintain 16:9 aspect ratio at all times
- h.264 or h.265 in an MP4 or MOV container

## Logos
AmadorUAVs has few logo variations, almost all based off of the classic hexagon AV. Below are some of the guidelines for use of the logo.  

### Text Logo with Icon
The AmadorUAVs icon with text is the standard logo, and can be used on black and white background. It can also be used as a negative for silkscreen, decals, or banners.

![AmadorUAVs Text Black on White](./logos/text-blackonwhite.png)
![AmadorUAVs Text White on Black](./logos/text-whiteonblack.png)

See the 'Typography' section below for information on approved fonts. 

### Icon Logo
The AmadorUAVs hex icon can be used as an identifier, but may not be used without context. 

![AmadorUAVs Icon Black on White](./logos/black-onwhite.png)
![AmadorUAVs Icon White on Black](./logos/white-onblack.png)

![AmadorUAVs Icon Gradient on White](./logos/gradient-onwhite.png)
![AmadorUAVs Icon White on Gradient](./logos/white-ongradient.png)

### Outline Logo
The outline logo is simply a variation on the hex icon. Do not use on busy backgrounds.

![AmadorUAVs Outline Logo](./logos/outline-variations.png)

### Inappropriate Use

Do not attempt to recreate the AmadorUAVs Logo with unapproved typography.
![Recreated AmadorUAVs Logo](./logos/recreate.png)

Do not stretch or skew the proportion of the logo in any way.

![Stretched AmadorUAVs Logo](./logos/stretch.png)

Do not tilt or rotate the AmadorUAVs Logo.

![Tilted AmadorUAVs Logo](./logos/tilt.png)

## Typography
Standard font for the club name is **Red Hat Display**, **Red Hat Text**, and as an alternative, **IBM Plex Sans**.

![AmadorUAVs Typography Info](./logos/typography.png)


## Colors

The AmadorUAVs gradient is described below:

![AmadorUAVs Gradient Info](./logos/gradient.png)


## Visuals

![AmadorUAVs Icon Background](./logos/icon-bg.png)

![AmadorUAVs Socials](./social.png)

## Email Signature (see [**Email Guidelines**](./email-guidelines.md))

When possible, scale down the gradient logo from "Original Size" to "Small"

> ![General Email Signature](./signature.png)

When sending an email personally, use the below guidelines for Name/Title/Phone Number. Phone number is not required, but adds a level of trust to the receiving party. 

> ![Personal Email Signature](./personal.png)

See the [**Email Guidelines**](email) for further information on email formatting. The below example may be used for informal emails, internal emails, or as replies to previously started email threads.

> ![Casual Email Signature](./casual.png)

## Social Media
