# Team and Project Management

## Project Management Guidelines
Before you start creating projects, analyze the true value.
* If they have value to you, they will probably have value to someone else; things that will draw almost no audience is probably not worth the time.

* Everybody's business is your business
    - Facilitate communication
    - Check in and update project timelines
    - Collaborate with team leads

### Effective Meeting Management
- Be prepared (self explanatory but also not)
- Those who are present to the meeting are part of the team - those who are absent are not
- Keep it moving but don’t cut people off
- Meeting should end with the team on the same page - communication between all will greatly improve efficiency and awareness
- Follow up even beyond the meeting
- Help establish goals
- Allow/encourage innovation
- Question and comment on new updates from individual members
- Different types of meetings
    - Large meetings (whole club, 25+ people)
        - Basically just people listening
        - A few people can represent a division or subteam to report on their progress
        - Everyone should have the opportunity to comment or suggest
    - Subteam meetings (entire division, ~10-20 people)
        - Still a bit of listening during the progress updates
        - Everyone gets a chance to contribute feedback and provide communication on their projects
- Keep in mind - most UAVs meetings involve separate small teams working on completely different projects - even though the end goal may be similar. Note composition of meeting members

- Types of meetings
    - Informative-digestive
        - Waste of time to talk about stuff that is found just in a doc
        - Could be useful if clarification is required
        - If it has deep implications for the members of the meeting
        - Receive and discuss a report, for example
        - Includes progress reports, and review upcoming projects to keep everyone updated
    - Constructive-originative
        - What should we do?
        - Collaboration, devising new thing, procedure, part, etc
        - Asks for member contribution of knowledge and experience
        - Will not work without presence and pitching in
- Executive responsibilities
    - How should we do it?
    - After members have decided how to go about doing something
    - Could be talk about tools, budget, timeline, planning
    - Many different considerations required
    - Formulate action plan at this point, keep everyone up to date
    - As an executive team, understand how each job fits in with other jobs, how each piece gets the goal moving forward

- Conducting meetings
    - What goal do you want to reach by the end?
    - Latecomers need to learn the lesson - if a decision is reached before their arrival they will learn quickly
    - Convey a sense of urgency
    - Take pains to commend contribution of junior members, take a point that they make and reintroduce to the team at the end or something
    - Close with a note of achievement

## Project Management

- KEEPING PROJECTS ON TRACK
    - DILIGENCE: Assign someone to keep track if it is too much for you to handle by yourself
    - Regularly check project progress
    - Frequent check-ins with members
    - Update chunk deadlines and push members to work faster
    - Team Performance
        - Weekly/monthly check-ins
        - Feedback
        - Manage performance with tools
- GETTING A PROJECT BACK ON TRACK
    - Cut parts of the project
    - Take work onto your own hands or give to other members who have more time
- **COMMUNICATE STATUS WITH REST OF THE CLUB**
- CREATE SHARED CALENDAR
