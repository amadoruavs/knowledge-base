# AmadorUAVs Business

## Objective Goal
The AmadorUAVs Business division's main goals are to: aquire and provide funding for all subdivisions to aid in research, development, and construction. Create and implement the AmadorUAVs brand using social media, in-person events, and educational opportunities. Build effective relationships with companies, organizations, and foundations for monetary, software, and hardware support and donation. 

### Why is business important in a robotics club?
**Business is the most important part of this organization** for a few reasons.

No business, no anything. Money has to enter the club in order for anything to happen. Even tasks as basic as asking your own parent to donate to the team, is a part of business. At what location can we have a meeting? That's a part of business too. How much money do we have for the next 6 months of development? A business member will know the answer to that. At the fundamental roots, the entire officer team is doing the work of business. From planning meeting to the overall timeline, this is a significant part of business. 

Why are business members trusted, and why do they have to be trustworthy? Finance, equipment, overall management lies in their hands. 


## Main responsibilities of Business Members (In order of importance)
1. Funding/sponsorship 
2. PR/Marcom (brand expansion, graphic design, can include media) 
3. Financial management 
4. Grants/fundraising 
5. Outreach/education 
6. Internal project/team management 
7. Maintain/manage business/foundation relationships 
8. Team expansion (in terms of event opportunities or even finding interested members)

## Important resources

### [Brand Guidelines](./brand-guidelines.md)
### [Email Guidelines (in progress)](./#)
### [Team/Project Management](./team-project-mgmt.md)
### [Sponsorship Guide (in progress)](./sponsor-info.md)
### [Logistics (in progress)](./logistics.md)
