# Logistics

AmadorUAVs is much more than just a drone club - it requires extensive resources to maintain a club of this size, especially with cross-country travel requirements for the SUAS competition. This section will outline the past, present, and future of AmadorUAVs logistics. 

## Equipment Storage

As of Fall 2022, 

## Equipment Transportation

### Injection Molded Cases

AmadorUAVs is sponsored by SKB Cases, a leading manufacturer of equipment protection transport cases based in Orange, CA. With expensive, industry standard equipment, comes incredible responsibility in terms of storage and transportation. AmadorUAVs utilizes a variety of SKB cases but standardizes with certain product lines for equipment protection.

| Case Type      | Description |
| ----------- | ----------- |
| Shock Racks      | 3RR-xU24 Series Removable Rack cases, 24 inch depth. Rack units can vary (x units)<br /><br />Current shock rack: [3RR-4U24-25B](https://www.skbcases.com/products/4-u-24-inch-deep-rr-series-shock-rack)|
| Standard Racks   | 1SKB-RxU Roto Rack cases, standard depth. Rack units can vary (x units)<br /><br />Current standard rack: [1SKB-R4U](https://www.skbcases.com/products/4-u-r-series-rack) |
| Single Lid Cases   | iSeries waterproof cases.<br /><br />Common sizes: [2011-7 (Carry-on capable)](https://www.skbcases.com/products/i-series-2011-7-case), [2217-12 (Checked bag capable)](https://www.skbcases.com/products/i-series-2217-12-case), [2615-10 (Checked bag capable)](https://www.skbcases.com/products/i-series-2615-10-case), [2317-14 (Checked bag capable)](https://www.skbcases.com/products/i-series-2317-14-case), [2918-14](https://www.skbcases.com/products/i-series-2918-14-case)<br /><br />Current single lid cases: [2918-14](https://www.skbcases.com/products/i-series-2918-14-case), [Peli 1620 (In need of repair)](https://www.pelican.com/us/en/product/cases/protector/1620?sku=1620-020-110)|
| Heavy Duty/Military Cases   | [rSeries](https://www.skbcases.com/collections/industrial-and-military/single-lid-cases/by-series/rseries) cases, varying sizes. Can be used for future pallet shipping of equipment |

### ATA Cases

For equipment that does not require waterproofing, AmadorUAVs also utilizes ATA Specification 300 equipment cases, also called **flight cases or road cases.** These are fabricated in-house with laminated plywood, aluminum extrusion, steel hardware and steel mandrel aluminum rivets. These cases are heavier and can be easily customized for use with specific pieces of equipment. 

The AmadorUAVs stock of ATA compliant cases is growing, but the current stock is outlined below. 

| Case Type      | Size        | Description |
| :---        |    :----   |     :----  |
| Lightweight Trunk    | Size code MED-LW A utilizes 1/4in plywood and stackable ball corners. LxWxH: 15x26x16 inches | Made to be air-shippable, similar size to a large Home Depot cardboard moving box |
| Large Storage        | Size code XL-HD A "*BBC*" LxWxH: 24x48x36 inches  | Currently the main tool/part/project storage case for any AmadorUAVs work area. Converted to work station in Nov 2022 |
| 3D Printer Transport | Size code VORON LxWxH: 24x48x36 inches  | For safe and efficient transport/storage of AmadorUAV's Voron 2.4 3D printer |

There will be more cases added during the 2022-2023 school year.

## Shipping

For the 2022 competition cycle, a variety of shipping methods were used to transport team equipment from CA to MD.

* Checked baggage: A total of 9 checked storage containers. 7x size large Home Depot moving boxes, 1x Pelican 1620, 1x 8" cardboard form tube = $315 + $140 on return
* Freight shipping: Freight shipping for the 156 pound SKB 3RR rack case housing the Cisco M4 server and CyberPower UPS = $409.24 + $619.15 on return
* USPS flat rate mailing: 5 Large Flat Rate boxes from USPS for battery shipping in Ammo boxes (Battery boxes) = $149.15

Total equipment transportation cost for 2022 competition: **$1632.54**

In the future, freight shipping can be a great option to send all equipment neccesary in a single trip. In this instance, the drone can actually be shipped fully assembled and ready for flight. 

Estimated cost (**PRIORITY**, Class 100 freight, 48x48x40, 400lb, $20,000 insurance): $987.45+$756.90 = **$1744.35**.

Without priority: *$1565.39*
