# AmadorUAVs Onboarding Handbook
Welcome to AmadorUAVs! AmadorUAVs is a student-led aerial robotics club that participates in a variety of robotics-based engineering competitions, as well as education, outreach, product development, and event management.

Under the support of Amador Valley High School, AmadorUAVs is a 170(c)(1) independently funded nonprofit organization.

### Start Here:
**You get out of this club what you put in.** Being a part of this team can certainly be the most exciting experience of your high school career but **you also have the opportunity to throw it away.** Make the most of what you have. 

### Goal
Provide students of all ages and interests the opportunity to design, develop, construct, and operate unmanned aerial vehicles (UAVs) of all types, and allow interested students to participate in events, competitions, and conferences. For highly dedicated and capable students, following a selective application process, provide the resources for this student team to develop, test, and integrate a highly capable unmanned aerial system (UAS) suitable for international competition. 

### Projects
Beyond UAS development and education, AmadorUAVs participates in a variety of additional events that may be of interest to students. Below is a comprehensive list of focuses students can choose.

* Competition UAS development based on competition objective (SUAS)
* Competition UAS development based on societal problem (NXP HG)
* Student education
* Conference travel (PX4, ROS, Hackster)
* Sponsor relations
* Strategic planning
* Website development
* Finance and purchasing
* Media planning/production
* FDM manufacturing and machine maintenance
* Hardware development and manufacturing
* Social media marketing
* Graphic design
* Digital/offline advertising
* Custom app development for internal UAVs infrastructure 
* Event management
* Asset/inventory management
* Logistics
* Equipment protection construction

## Officer Team
AmadorUAVs 2022-2023 Officer Team has 6 executive positions:

* President, Aayush Gupta
* Business Lead, Anya Jain
* Mechanical Lead, Rachel Schmidt
* Electrical Lead, Abe Kiziloglu
* Software Lead, Nikhil Sunkad
* Non-Competition Lead, Christopher Hazell

### Management Leads (tbd)
In addition to the executive leadership team, AmadorUAVs requires leadership positions on the division level. 2023-2024 team: Contact your division lead if you find interest in any of these roles. 
* General Team Management, 2023-2024 TBD
* Mechanical Project Coordination, 2023-2024 TBD
* Electrical Project Coordination, 2023-2024 TBD
* Business Project Coordination, 2023-2024 TBD
* Software Project Coordination, 2023-2024 TBD
* Team Dynamic Management, 2023-2024 TBD
* Equipment Management, 2023-2024 TBD

### Student Positions 
There are a variety of specific tasks that require individual expertise and management abilties.
* Graphic Design Coordination, Angad Bhargav
* Finance, 2023-2024 TBD
* Communication, 2023-2024 TBD
* Social Media, Anya Jain
* Website Management, Vardaan Singhania

## Operation Guidelines
### In-Person
It is extremely important to practice good planning, organization and execution when working on physical projects in the AmadorUAVs "shop" to improve efficiency, reduce part loss, and most importantly, personal injury. 

* **Do not work any hazardous equipment alone.** This includes cutting tools, power tools, and drone motors. 
* Proper PPE is required when neccesary
* Follow all standard operation procedures (SOPs) when provided
* Listen to the assigned team lead before, during, and after work
* A clean work area is the only way the work area should be left. Regardless of the project, small parts must be kept safely in bags/storage containers and organized by project before departing. 
* **After the in-person work is complete, communicate progress to the division/project lead to update the entire team.**

### Online
* **Check Discord.** More specifically, check #news and the Events tab.
* There will not be many lengthy voice calls, so pay attention and stay engaged during online meetings
* Common chat courtesy is expected. (And should not have to be explained)

### Team Behavior
As with any other school-based team, good behaivor is expected at any event, on- or off-campus. Your behaivor is a direct representation of the entire team and must maintain a high standard.

However, within the team, there are important behaivoral considerations that must be made:
* See challenges as opportunities
* Have a good attitude
* Be on time to meetings
* Clean up after yourself
* Ask if you have questions or don't know something
* **Commit to only what you can deliver**

## Productivity and Management Tools
More than a club, AmadorUAVs is an intricate multidisciplnary operation that requires collaboration from all members to improve productivity and management on all levels. These tools **MUST** be used in order to provide any purpose or benefit. 

### Master Plan
The 2023 AmadorUAVs competition team utilizes a synchronous Google Sheet to create, assign, manage, and track tasks, as well as produce timelines, schedules, task boards, and calendars. 

**Contact your division lead for access to the competition team Master Plan**

### PartKeepr
PartKeepr is used for tracking component stock, location, and BOM management, especially useful for custom circuit boards (PCBs).

Parts that are entered into PartKeepr should be considered consumable, and have a depletable stock. Some common items include:
* Electronic components
    - Passives, discrete circuits, ICs, LEDs, etc
    - Connectors
    - Wire
* Fasteners (Screws, bolts, nuts, washers, etc)
* Building materials
    - 3M VHB (very high bond) double-sided tape
    - Threadlocker
    - Glue
    - Paint
    - Solder
* Raw materials
    - Fishing line
    - Rope
    - Wood
    - Plastic
    - Aluminum
* Select tools
    - Drill and driver bits
    - Individual hex keys

### Assets
As of October 2023, AmadorUAVs currently uses a spreadsheet as the official asset management solution. 

When determining when to add some*thing* into either PartKeepr or Assets, keep in mind that assets in AmadorUAVs are considered fixed. This includes: 
* Reusable storage containers 
    - Molded cases, racks, etc
    - Flight Cases
    - Part bins
    - Straight wall containers
    - Component organizers
* Electronic equipment (not basic components)
    - Computers, monitors, printers
    - Chargers, ground support equipment
    - Electrical test equipment
    - Two-way radios
* Tools
    - Power tools
    - Hand tools
    - Soldering and rework equipment
* Select UAV Hardware (of significant value)
    - Flight controllers
    - LiPo batteries
    - Radios
    - Embedded electronics
* AVHS Events Equipment

### Knowledge Base
What you're reading right now, the AmadorUAVs Knowledge Base, is a documentation page built by AmadorUAVs members, past and present. Navigate to the [Welcome](https://knowledge.amadoruavs.com/) page for information on contributing to it. This documentation is written in Markdown, a lightweight markup language for formatting text.

### Google Drive
AmadorUAVs uses Google Drive for relevant filesharing of images, videos, documents, and forms. 

## Very Useful Links
* [AmadorUAVs Website](https://amadoruavs.com/)
* [Permanent Discord Invite](https://discord.gg/y74SmPt4Mk)
* [Google Drive](https://drive.google.com/drive/folders/16nHqNi4ZVFqhAchsPvPFHWDqQ4Yzr0Ja)
* [ClickUp](https://app.clickup.com/)
* [PartKeepr](https://parts.amadoruavs.com/)
* [AmadorUAVs Assets](https://assets.amadoruavs.com/)

