# Gimble
The AmadorUAVs Gimble project aims to engineer a simple, open-source
gimbal hardware and firmware solution for camera gimbals.

Gimble is a quasi-side project; it is intended to be used for competition.
While there is a backup plan in case Gimble is not developed in time,
Gimble would be superior to said backup plan.

Current tasks:
- Design boards
- Write brushless motor control firmware
