# Macron at a Glance

## Overview
Macron is an octocopter. That means 8 motor/propeller pairs, usually on 8 separate arms. Each motor is controlled by an ESC (electronic speed controller), which interfaces with the FC (flight controller), which runs the show. The FC is the brains of the drone, responsible for controlling the motors to achieve stable flight. It also interfaces with other components on the drone, executes autonomous flight plans, and communicates with the GCS (ground control station). Other articles will go over how to operate these components in more detail.

## Where to get designs
The entire hardware design for Macron is modeled and available to club members on OnShape (online CAD suite). All custom software that runs on the drone can be found at https://gitlab.com/amadoruavs. Contact the division leads for more details.

## Frame
Macron is based on a Freefly CineStar 8 frame (produced in 2013), which was donated to the AVHS engineering department after it was retired from production use. The frame is fully carbon fiber, with 8 cylindrical roll-wrapped tubes as arms, and plate legs attached to 2 central octagonal CF plates. At time of writing, the original legs have been replaced with 2 custom tube legs, which are T-shaped CF tubes attached to a tee, mounted to opposite arms. Macron also has various 3D-printed attachments, including a main electronics plate (named Fred) that is mounted on top of the central octagonal plate and houses most of the avionics and radio equipment. *Remember to only print Fred in RED*.

## Flight Controller
At time of writing, Macron's flight controller is a CUAV Nora. Fast processor, redundant sensors, bla, bla - I'll save the implementation details for the knowledge-base - but it's a pretty good FC. It runs [PX4](https://px4.io), a professional and open source *autopilot*. The autopilot is responsible for all the flight control, radio communication, telemetry, autonomous navigation, etc.

## Propulsion/drive
The motors used are [XOAR Titan Air TA-6012 130KV](https://shop.xoarintl.com/products/titan-air-ta6012-130kv-260kv) motors, which are driven at 12S voltage (more on that later) with 24x9 inch CF propellers (also from XOAR). The ESCs (which drive the motors) are [Zubax Myxa A ESCs](https://zubax.com/products/myxa). The ESCs are mounted in a 3D-printed enclosure, which is then attached on the underside of the arm beneath the motor. Note that when using 8 arms, these propellers are way too large to all fit on the top - so every other motor is instead inverted into a pusher configuration.

## Payload
### UGV
For the SUAS competition, Macron has a unmanned ground vehicle (UGV) attached under it. At time of writing, details for this have not been finalized, but they will follow in future articles. Stay tuned!

### Competition Camera
For the SUAS competition, an [Imaging Source industrial camera](https://www.theimagingsource.com/products/industrial-cameras/usb-3.0-color/dfk33ux183/) is used. This is either mounted directly to an arm or on a small gimbal - details TBD.

### Gimbal
For filming/streaming, Macron has a payload mount for a large Gremsy T3v2 gimbal, which can then be used with a DSLR for epic footage.
