# Terminology
Below is a list of commonly used terms here at AmadorUAVs - basically the AmadorUAVs glossary.

| Term        | Description |
| ----------- | ----------- |
| UAV | Unmanned aerial vehicle. The unmanned flight vehicle itself - it can be a multirotor, fixed-wing, or VTOL type vehicle. |
| UAS | Unmanned aerial system. Air vehicles and associated equipment that do not carry a human operator, but instead are remotely piloted or fly autonomously. |
| SUAS | The Student Unmanned Aerial System competition. The competition is designed to foster interest in Unmanned Aerial Systems (UAS), stimulate interest in UAS technologies and careers, and to engage students in a challenging mission. |
| UGV | Unmanned ground vehicle. Another word for an RC rover, used in the 2022 competition. In the context of the SUAS competition, the ground vehicle is fully autnonmous |
| GCS | Ground control station. Encompasses all components of the ground station required for the SUAS competition. This can include, but is not limited to the main mission computer/laptop, imaging server, backup power systems, ground networking, and cabling/interconnection. |
| Antenna Tracker | The motor-guided, sensor-based directional antenna positioning system used for the 2022 competition. Utilizes stepper motors, a 3D printed frame, and carbon fiber tube structures. Mounted on a heavy duty speaker stand. |
| Gimble | Custom gimbal project in development by the AmadorUAVs team. |
| Telemetry | Telemetry refers to the real-time data, flight readings, and vehicle information that is sent and received by the flight vehicle. Typically a wireless MAVLink connection between a ground control computer and flight vehicle utilizing telemetry radio modules. |
| RC | Remote control. Can refer to RC radio, or as a general term for RC Vehicles (cars, boats, tanks, etc). | 
| PX4 | PX4 is an open source flight control software for drones and other unmanned vehicles. **Note: PX4 is not a company or organization.** |
| Dronecode | The Dronecode Foundation is a vendor-neutral foundation for open source drone projects. A US-based non-profit under the Linux Foundation and provide open source governance, infrastructure, and services to software & hardware projects. |
| Pixhawk | Pixhawk is an open hardware standard for drone electronics. **Pixhawk is not a software, company, organization, or hardware manufacturer.** There are however, certain models built on the Pixhawk standard that share this name, such as the Pixhawk 1, originally built by 3DR, or the Pixhawk 4, currently built and sold by Holybro. |
