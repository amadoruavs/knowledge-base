<div id="frontmatter" style="display: none">
title: "AUVSI SUAS Competition"
</div>

# AUVSI SUAS Competition

The AUVSI SUAS international collegiate drone competition is
an annual competition held in Maryland. Tasks include
following a set of waypoints, avoiding virtual obstacles and
other drones, surveying and identifying ground objects, and
mapping ground areas.

[The competition website is here.](https://suas-competition.org/)

[The 2023 Rule book is here. IMPORTANT - PLEASE READ.](https://suas-competition.org/s/suas-2023-rules.pdf)

After you've familiarized yourself with the nature of the competition
through the sources above, proceed to [Competition Stack](compstack.md).
