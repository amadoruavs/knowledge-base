# Non-Competition Projects
Our club has several non-competition projects in addition to the main
competition team. These projects are self-planned by members, have no
external deadline and are open to any member to join.

For info on each specific project, see the specific categories in the sidebar.
