# Project Mikro
Project Mikro is our team's internal project to develop an inexpensive,
mass-manufacturable autonomous drone out of commercially available parts.
The drone will be used for swarm and education purposes.

## Design
Project Mikro is designed to run on small brushed hobbyist motors, in
order to reduce costs. Because of this, the drone has to be extremely
light. The entire drone, with all components attached, weighs around 70g.

Project Mikro uses 4 8520 motors with 65mm props, generating a collective
thrust of around 160g.

Project Mikro's main development effort goes towards developing a custom
flight controller board to run the flight control software. The flight
controller is currently in its third iteration and is awaiting manufacture.

## Todo Tasks
These tasks are open to any member to take up.
Ask coder.kalyan@gmail.com or vwangsf@gmail.com for details.

- Review MicroFCv3 board
- Print new frame
- Build new prototype
- Build documentation website for Mikro education

