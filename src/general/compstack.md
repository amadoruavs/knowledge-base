<div id="frontmatter" style="display: none">
title: "Full Competition System Stack"
</div>

# Full Competition System Stack

![Competition Stack](../images/compstack.png)

Note: If you do not understand any terminology,
check out [Terminology](terminology.md).

The AmadorUAVs Competition Stack consists of many parts.
Let's break it down.

## Boreas
Boreas is our team's main competition drone as of 2022. It handles following
waypoints, capturing images etc. Boreas is a coaxial quadcopter
(4 arms, 2 motors on each side of the arm) running the PX4 open-source
autopilot.

### Pixhawk 5X 
The Holybro Pixhawk 5X is Boreas's flight controller. It handles receiving commands
from the radio link and controlling the motors. In addition, internal sensors facilitate fully autonomous flight utilizing the PX4 software stack.


## Interop Server
As of the release of the 2023 competition rules, the Interop server has been removed. In the past, the Interop server contained mission data, and is
the place where ground objects and drone telemetry must be uploaded.

## Ground Control Station
The Ground Control Station is the computer(s) that runs all ground-based
software.

### QGroundControl
QGroundControl is the Ground Control Software (GCS) used to control Boreas.
It communicates via a telemetry radio link to the flight controller
to execute missions, read back drone data, etc.

QGroundControl is fed a path by Pilot for the competition run.

### Pilot
Pilot is our path planning system. It reads mission descriptions from the
Interop server and generates a corresponding .plan file, which can be used
by QGC.

### IMS - TBD
In 2022, IMS was our object classification system for ODLCs (Object Detection, Localization,
Classification). It consisted of a frontend for manual classification and
review, as well as a backend server which stored images uploaded from the drone's
Photographer module.

## Jetson Nano
The Nvidia Jetson Nano is the companion computer that runs on the drone. It is used for
identifying regions of interest for ODLCs and for some control tasks.

### Imaging Source Camera
The Imaging Source DFK 33UX183 is the industrial camera onboard the drone. It features the Sony IMX183, a 1-inch, 20MP color sensor with rolling shutter. With 12 bits of dynamic range, the camera can record images at full resolution up to 18 times per second (18fps). The camera utilizes USB 3.0 connectivity to the companion computer. The lens on the camera is a Computar V3522-MPZ C-mount machine vision lens. It has a fixed focal length of 35mm, a maximum aperture of F2.2, and offers 0.0% distortion when used with a 1-inch image sensor. 

### Photographer - TBD
In 2022, Photographer was the drone-side image capturing software. It grabs images
from the camera, runs region-of-interest identification on it, and uploads
regions of interest to the IMS server.

## Antenna Tracker - TBD
At the 2022 SUAS competition, the Antenna Tracker was utilized to point the directional ground antenna towards the
drone in order to achieve a stable, high-speed link. The Ubiquiti Powerbeam AC was secured with a custom 3D printed mount and used stepper motors to control yaw and tilt.

### Powerbeam - TBD
The Ubiquiti Powerbeam mounted on the antenna tracker provides a WiFi connection to
the drone from ground.

### Bullet
The Ubiquiti Bullet mounted on the drone receives a WiFi connection on
the drone.

## UGV - TBD
For the 2022 competition, the UGV is the ground vehicle that needed to be dropped from the drone.
It must land in a certain area, then drive to a different area. It was controlled by a Pixhawk 4 flight controller + a microcontroller which
translates flight controller signals into motor driver signals. However, after a variety of technical issues, the driving functionality of the UGV was sacrificed. 
