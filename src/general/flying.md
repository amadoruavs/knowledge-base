# Flying
This document lists various notes and tutorials about flying autonomous drones. This is a general set of instructions that can be adapted from drone to drone and will give general guidance on preparing the UAS for flight.

## Pre-requisites
This assumes that the entire drone is assembled, and the flight controller is calibrated and configured (steps to outline this are outlined in another document).

Depending on flight location, prior airspace clearance may be neccesary for lawful  flight. This is typically done using the Aloft Air Control app, which is an FAA approved LAANC (Low Altitude Authorization and Notification Capability) provider. As of Fall 2022, LAANC is still being used as a collaboration between the FAA and the small UAS industry to offer drones controlled airspace at 400 feet or below. 

## Batteries
Regardless of battery configuration, charge (battery voltage) must be measured before flight and monitored during use for safe operation and efficient flight. Failure to do this can lead in saturation of one or more motors which leads to instability and crash. When possible, use the same brand/pair in series (these pairs are labeled as {1-8}{A,B}).

## Telemetry Radio
Telemetry radio is one of the two radio required for flight. The telemetry radio utilizes two units: one onboard the drone the transmits and receives flight data, and the other is connected via USB to the ground control computer. Depending on which model of telemetry radio is being utilized, there are some specific setup requirements that are outlined below:

### Holybro Microhard P900
Each telemetry radio must be connected to external power provided through the XT30 to 4-pin JST-GH battery connector provided with each radio. Specifically for the ground unit, the USB-C port on the module can be connected directly to the ground control computer with a data-capable USB-C cable. After receiving external power, the remote (drone) module can be connected to the flight controller (typically TELEM1) with the 6-pin JST-GH telemetry cable.

### Holybro SiK Telemetry Radio V3
The Holybro SiK Telemetry Radio V3 units are lightweight, reliable, and inexpensive, and are typically shipped with S500/X500 kits and Pixhawk Flight Controller bundles. As usual, one is wired to TELEM1 on the flight controller, while the other is wired through Micro USB to the ground control station. This radio requires no external power, making it versatile in a pinch. 

### RFDesign RFD900x (Deprecated)
Similar to both of the above telemetry units, one module is needed per vehicle, and one unit is needed to connect to the ground station. The RFD900x does not require external power, but uses a custom cable to connect from the 2.54mm pins to USB for the ground station and JST-GH for the air unit. For the ground station, connect the radio to an FTDI USB-TTL cable (which converts the UART on the telemetry radio) to a USB CDCACM device on the ground control computer. Use the wiring diagram below as a reference when connecting the 6 pin header.

## RC Radio
The RC radio, also called the handheld transmitter, or just *transmitter* is the direct human input device used to control the drone. If not connected already, a typical RC radio receivers has a servo rail capable of connecting to control surfaces, but also a single output, such as SBUS or PPM. This output is what is connected to the flight controller through a Dupont to Dupont 3 pin servo wire.

RC setup, including binding, depends on the RC being used, but power up the transmitter before flight and ensure the safety pilot (Pilot in Command/PIC) has a visual of the aircraft at all times in case of autonomous guidance failure. In certain situations, it may be neccesary to have the roles of PIC and person operating the controls separate - in that situation, the PIC must have direct supervision of the person operating the controls at all times, and be capable of taking over at any time. 

## RTK
AmadorUAVs utilizes the Holybro H-RTK F9P GPS system. Set up the RTK ground station by connecting the RTK base antenna to the F9P ground unit with the SMA male antenna connector. Next, connect the RTK base unit to the ground control computer via USB. With the ground control software running, the RTK base will enter survey-in mode - this a startup procedure to get an accurate position estimate of the base station. It may take several minutes to complete. Once the RTK GPS icon turns white, the system is ready to use. 

## Bootup
Connect all batteries to wiring harnesses before powering the power distribution system. Plug in the final series/parallel harness to the PDB. All components (ESCs, flight controller, peripherals, Jetson Nano, and Wifi systems) should boot up. Open QGroundControl on the ground control computer. The telemetry radio should connect to the drone, and QGC will request a parameter list from the flight controller (demonstrated by a green progress bar on the top of the screen) before rendering the rest of its UI. Note that to display the map, you will need internet access on the main computer. If flying in a location without internet access, download an offline map ahead of time. If using RTK, QGC will automatically start the Survey-In process, and the RTK symbol will be displayed in red. When this process is complete, the symbol will become white and say "RTK Streaming".

## Flight Checks
The flight controller will start in a manual flight mode such as Manual/Stabilized. Once sufficient GPS lock is established, the FC will automatically switch to Position mode and play a tone. You can then attempt to arm the drone, either via QGC or the RC. If there are any pre-flight checks that fail, fix them. The drone should then arm.

## Flying
You can then proceed to fly the drone in a mode of choice. More information on flight modes is outlined in another document, but here's a quick introduction:
* Acro does not self level, so it is not recommended for large drones. **ACRO SHOULD NOT BE USED ON ANY AmadorUAVs PRODUCTION DRONE**
* Manual self levels when the sticks are released, but is otherwise completely manual control and does not do any assistance.
* Altitude maintains altitude lock but does not hold position horizontally.
* Position levels and holds position when sticks are released, and also limits tilt angle and ascend/descent velocity. **Typically, position mode should be used for manual flight during testing.**
* Mission can be used to fly a mission.
* Other flight modes are used for more specific (autonomous) use cases.
