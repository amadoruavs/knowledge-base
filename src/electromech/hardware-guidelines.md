# AmadorUAVs Mechanical Design and Assembly Standards
Updated May 2022

## Design
### Fasteners
AmadorUAVs standardizes on M3 and M4 hardware for general assembly purposes. Fasteners are 18-8 Stainless by default. 

https://www.mcmaster.com/91292A268/

https://www.mcmaster.com/93033A105/

https://www.mcmaster.com/93033A107/


