# Advanced Schematic Design and Choosing Components

In the previous guide, we made a simple ADC board, but real boards
are nowhere near as simple. In this guide, we'll learn to choose
real components for a variety of use cases:
- Microcontrollers
- Connectors
- Regulators
- Transceivers
- Sensors

## Digital Schematic Design

Digital schematic design generally involves a microcontroller and components
around the controller, such as sensors. They come with power requirements and
restrictions on what pins can be used for what.

### Planning a Design

As always, before starting a board design we need a list of requirements.
For this specific board, we'll design a [gimbal IMU module](https://gitlab.com/amadoruavs/gimble/imu-module). This module is necessary for a 3-axis gimbal to perform
well, because it needs to be able to measure the angle of both its base
and the actual camera to accurately hold angles.

Basically, our board design will:
- Take in sensor input from an IMU (Inertial Measurement Unit)

### Choosing a Microcontroller

The central piece of a digital board is the MCU. Three categories generally
exist for boards like ours:
- ARM32-based MCUs (STM32, ATSAMD, etc.; Blue/Black Pill series)
- AVR-based MCUs (Atmega328p, Atmega2560; Arduino board series)
- Miscellaneous other, specialized MCUs (e.g. ESP32)

The club's standard catalog of microcontroller types is located in the
[Standard Components Library](https://gitlab.com/amadoruavs/electrical/standard-libs).
