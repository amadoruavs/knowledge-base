# PCB Layout

PCB layout is the fun part, because it's how you take your schematic
and make it into a real-life board.

## Assigning Footprints

The first step of layout actually starts in the schematic editor. You
need to tell KiCad what the abstract schematic components actually
look like in real life; basically, you need to tell KiCad the space that
the component takes up on the board. These are called *footprints* - the
*footprint* of the component on the board.

To start, go to Tools -> Assign Footprints. It'll ask you to annotate
components - remember, this numbers off your schematic components so
KiCad can identify them:

![Annotate](images/annotate.png)

Simply click "Annotate". Once the board is annotated, the *Assign Footprints*
dialog will be displayed:

![Assign Footprints](images/assign-footprints.png)

Let's assign the footprints to this board.

### A Brief Aside: Standard SMD/THT Component Sizes

Remember that SMD components take up one side of the board, while
THT components drill holes through both sides of the board.

SMD components usually have very standard sizes. THT components
are generally more flexible, but still have standard sizes e.g.
for connectors.

#### SMD Components

Rectangular components such as resistors, capacitors and LEDs
come in standard sizes:
- 0201 (0.6x0.3mm)
- 0402 (1.0x0.5mm)
- 0603 (1.6x0.8mm)
- 0805 (2.0x1.2mm)
- 1206 (3.2x1.6mm)
- 1210 (3.2x2.5mm)

These sizes are the standard dimensions in inches (e.g. 0805 resistors
are .08"x.05").

Many integrated circuits and other components are also standard:
- MOSFETs and SMD transistors almost always come in *SOT-23* (3 pins).
- Regulators usually come in *SOT-23-5*, a SOT-23 variant with 5 pins.
- Many ICs (transceivers) come in either SOIC-8 or SOIC-16 formats.
- ***Diodes (not LEDs) usually come in SOD-123 or SOD-323, NOT standard 0603/0805***. I madethis mistake once. Don't repeat it.

![SMD Footprint Sizes](images/footprint-sizes.png)

Horizontal common connectors:
- JST series (most commonly JST-GH on small boards)
- A ton of different Molex connectors

#### THT Components

THT components are usually less standard because the through hole nature
means you can use a variety of hole layouts. However, some components are standard:

- 2.54mm pinheaders (DuPont pins) are by far the most common connector. They fit on breadboards and perfboards.
- 5.08mm (or less commonly 3.5 and 5.00mm) terminal blocks are plastic blocks that you can screw wires into for a durable connection. They are also commonly soldered to a THT through-hole pad.
- DIP (Dual In-line Package) components are 2.54mm/0.1inch spaced pins with two rows of through-hole pins running parallel (like the most common variant of the Atmega328P). These usually go into DIP holders, although they can also be directly mounted to PCB holes.

![DIP package](images/dip.png)

### Choosing Our Footprints

To find the right footprint, we obviously need to choose the actual
physical components we are using. For that, we need to consult electronics
components distributors; the largest US-based ones are [Digi-key](https://www.digikey.com/)
and [Mouser](https://www.mouser.com/). We will go into more detail
about sourcing components in the [Advanced Schematic guide](schematic-advanced.md).

Let's choose an SMD diode to use:

![SMD Diode Search](images/diode-search-1.png)

![Found one](images/diode-search-2.png)

This diode uses the 1206 footprint, so we'll add that by selecting the
Diode_SMD library on the left, then selecting the unfilled D components,
then finding 1206 on the right:

![Diode Footprint](images/diode-footprint.png)

Next, we'll do our connectors. We want to use 5.08mm terminal blocks, so
we'll choose the somewhat generic Phoenix 5.08mm connector footprint:

![Termblock Footprint](images/tblock-footprint.png)

Now hit OK to save our footprints.

## Placing the Footprints

Now, let's get our footprints onto the actual board layout.
Go over to the *PCB editor* and go to Tools -> Update PCB from Schematic:

![Update PCB](images/update-pcb.png)

Click "Update PCB" and the footprints we assigned earlier will show up
in the editor:

![Placed footprints](images/placed-footprints.png)

These are your physical components on your physical board.
Note the thin white lines connecting pads to each other. This is called
the *ratsnest*; it basically tells us what connections still need to be
formed between which pads on which components. For example, the VCC pad
on one resistor needs to be connected to the VCC on the output connector.
The ratsnest will eventually hide itself once your board is laid out,
but you can also hide it in the *Objects* tab on the right sidebar:

![Ratsnest](images/ratsnest.png)

Now, let's arrange the components to look more reasonable:

![Rectifier Board Example](images/rect-board-ex.png)

This looks much more compact. Notice how the ratsnest is more organized;
generally, less crossed ratsnest wires means a cleaner, less painful layout.
Remember, you decide how your schematic and layout look, so make it as painless
as possible.

The next step is to draw a *board outline* around the board, to tell the manufacturer
where the board stops. To do this, switch to the *Edge.Cuts* layer and select
the *Rectangle* tool, then draw a rectangle around the board outline:

![Edgecuts](images/edgecuts.png)

Now we've defined our board!

KiCad has a neat 3D visualizer for our board layout that can be activated by going
to View -> 3D Viewer. Let's take a look at our board:

![in 3d](images/3dwoah.png)

That's how our board will look in real life. Pretty neat, right?

We've already completed the primary *layout* portion of the layout - placing down
the components and creating the board outline. Our last step is to create the
*traces* connecting the components.
