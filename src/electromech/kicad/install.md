# Installing KiCAD

## Installation

[https://www.kicad.org/download/](https://www.kicad.org/download/).
It's not too hard.

## Navigating the Software

Your KiCAD should look something like this:
![Base KiCAD](images/kicad-empty.png)

Note several aspects:
- The *project tree view* is on the left. This is where you will access your schematic files, PCB design files, and more.
- The *symbol editor* is on the right. We will eventually use this to create custom schematic symbols.
- The *footprint editor* is on the right. We will never use this.
- The *image converter* is on the right. We will use this for PCB art.
- Under **Preferences**, there are options to "Manage Symbol Libraries" and "Manage Footprint Libraries". We will use this to add custom symbols and footprints later.

Proceed to [Creating a Project](creation.md) once you are somewhat familiar with the base interface.
