# PCB Creation

To create a Kicad PCB project, follow these steps:

1. (Optional) create a Github repository to store the files.
2. (See 1) clone the repository. If you aren't using a Github repo, make a folder instead to contain your files.
3. Go to File -> New Project. Navigate to the cloned repository or the folder. Deselect "Create new folder for project" and confirm.

![Creation dialog](images/kicad-create.png)

4. You should now have a new project that looks something like this:

![New Project](images/newproject.png)

These are your *schematic* and your *board layout* files.
Let's talk about what exactly these are.

## What is a schematic?

A *schematic* is an abstract representation of your electrical circuits.
It's basically an electrical diagram. Here's an example schematic of
an LED, hooked up to a common 555 timer, which should cause the LED to
blink regularly:

![schematic example](images/schematic-example.png)

## What is a board layout?

A *board layout* describes where your components physically go on the PCB.
It is typically created from the schematic, and tied to the schematic.

![board example](images/baord-example.png)

Proceed through both tutorials to learn more about editing each type of file.
