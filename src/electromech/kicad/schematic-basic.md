# Schematic Design

The *schematic* is probably the most important part of your PCB; it describes
every component and electrical connection you have on the board in abstract,
conceptual diagrams. Let's talk about how to use KiCAD's Schematic Editor.

This guide assumes you have already [created a project](creation.md).

## A View of the Schematic Editor

Make sure the project we've created is open.

To open the Schematic Editor, either click on the .kicad_sch file
or the Schematic Editor button:

![Opening Editor](images/open-schematic.png)

Once you've opened the editor, you should be greeted with a view that
looks like this.

![Schematic Editor](images/schematic-editor.png)

Don't worry about the unfamiliar terms used to label the parts on this diagram.
We'll explain them all in this guide.

## Planning the Concept

It's pretty important that you have an idea of what kind of board
you want to design. Try making a bullet point list of all the requirements
for your board (connectors, IO types, required components etc).

For this example, we'll be making an AC rectifier board. It will:
- take in AC input through a 2-pin connector
- push out DC output through a 2-pin connector
- have diodes in the middle

## Creating Your First Circuit

Let's create a simple AC rectifier schematic:

![AC rectifier example](images/ac_rectifier.gif)

This rectifier takes in AC (alternating current) inputs and converts them into
a DC (direct current) output. Since AC power oscillates both directions,
the direction of current flow switches quickly; the diodes force the current
to flow in one direction.

To create a schematic of a rectifier, we first need to add a *symbol* for a diode.
This is an abstract representation of a real-life diode.

Hit the "a" key or press the "Add symbol" button on the right sidebar. You
should get a menu with symbols to select from:

![symbol menu a](images/symbol-menu-a.png)

Type in "D" for diode:

![Choosing a symbol](images/symbol-menu-choose.png)

Select the diode. It should spawn into the schematic, and you should be able to
place it down.

![symbol analysis](images/symbol-analysis.png)

Let's take a look at the symbol:
- The symbol drawing allows you to easily identify it as a diode. Current can only move forward (in the direction the triangle is pointing), not backward.
- The D is a stand-in for the value of the symbol. In resistors or capacitors, these would be the resistance/capacitance values. Diodes don't really have values, so we leave it as D.
- The D? indicates the *reference designator*, which is basically the ID number of the diode. KiCAD uses this to know which diode is which. We will use a tool to automatically fill out the reference designator later.

For now, let's place down 4 more diodes. Either select them again from the "Symbol Add" menu or just select the diode, copy paste with Ctrl+C and Ctrl+V, and arrange them in a rectifier pattern.

![unwired rectifier](images/rectifier-unwired.png)

This is great and all, but we need to actually wire up the diodes and add the inputs/outputs. To create a wire, hit the "w" key to start wiring:

![wiring mode](images/wiring-mode.png)

and connect the diodes together at their pins. You can usually click on the end pin to
finish. If you want to wire it somewhere other than an end pin, press "k" to finish the wire.

![rectifier no input](images/rectifier-noinput.png)

This is what it should look like wired. Looks good, but we don't have any inputs for the AC
or outputs for the DC yet!

Let's create a *label* for the AC phases:
![Rectifier with Labels](images/rectifier-labels.png)

This will create a *net* for AC_IN_A and AC_IN_B.
We've also added *power nets* for VCC (DC+) and GND (ground).

### But what are nets?
The best way to understand is to demonstrate.

![Net Demo](images/net-demo.png)

All labels belonging to the same *net* are invisibly connected.
That's why they're called *nets*: When you draw the invisible
lines between the labels, it forms something that looks like a
net.

### Finishing Up the Rectifier

Let's add two connectors. Remember, to add components, hit the "a" key to
open up the Symbol Add menu, and search for `01x02`. `Conn_01x02` should
pop up.

Add two connectors, and, you guessed it, add the labels:

![Rectifier With Connectors](rectifier-with-connectors.png)

You can also use the specific screw terminal connector instead of the
generic connector, but it doesn't matter beyond giving other engineers
more info.

Congratulations - you've just made a basic schematic!

One more step:

### Running the Electrical Rules Checker (ERC)

The ERC is an important step in your schematic; it does automatic
checks to make sure your configuration is electrically valid (so you
don't have missing connections or power inputs).

To run it, go to Tools -> Annotate Schematic (this numbers off your
components so KiCad can identify them). Then, go to Inspect -> Electrical
Rules Checker and press "Run ERC".

![ERC](images/erc.png)

The ERC is complaining that we don't have any power inputs, because we don't:
KiCad has no idea where our power input is coming from. To fix this, we need
to add a PWR_FLAG to the source of power:

![PWRFLAG](images/pwrflag.png)

Now ERC passes. This board was very simple, so ERC wasn't super important, but
with complex digital schematics with multiple ICs, it will become very useful.

Now go on to [PCB Layout](layout.md) to turn this schematic into a real board.

### BONUS

Try adding in a power status LED. You'll need a VCC, a resistor,
an LED, and a GND.
