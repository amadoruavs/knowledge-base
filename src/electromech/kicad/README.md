# PCB Design

What is a PCB? What is a design? How do I PCB? Who is JiaLiChuang and why do they want to take my money?

All this and more answered in this guide to PCB design.

## What is a PCB, and why should I design one?

A PCB is a *printed circuit board*. If you open up any electronic device, you'll
likely see a thin board with a bunch of electronic components on it; that's a PCB.

To consider why we should design a PCB, we must consider the alternatives:
- Breadboard
- Perfboard (perforated board)
- Just like manually soldering stuff together

PCBs, being directly soldered, are far more durable than breadboards, which plug in with friction-fit pins. They are also generally stronger than manual, non-board mounted direct soldering of resistors etc. because there's actually a place to mount the components to.

PCBs also have the advantage of being able to use *SMD (surface mount device)* components,
which mount to one side of the board. Perfboards and breadboards require THT (through hole)
components, which are generally larger and more bulky. This allows designs to be made
much smaller.

## Important Terminology

A few pictures are worth a thousand words.

![Schematics and Layouts](images/terminology-project.png)
![Schematic Symbol](images/terminology-schematic.png)

## How do I design a PCB?

Designing a PCB usually proceeds through four steps:

- [Creating a project using EDA (electronic design automation) software (e.g. KiCAD)](creation.md)
- [Creating an electrical schematic for all the components](schematic.md)
- [Creating a PCB layout (physical locations of components on the board) from the schematic](layout.md)
- [Exporting assembly files (gerbers) and sending them to a manufacturing house (e.g. JLCPCB)](export.md)

The rest of this guide deals with each step individually.
Proceed to [Setting up KiCAD](install.md) to learn more.
