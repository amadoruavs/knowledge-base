# CAD Workflow Guidelines
Updated by Ishan Duriseti, September 2021

## General Rules
* **DO THE ONSHAPE LEARNING COURSES<br>FINISHED? DO MORE.**
* DID YOU KNOW? THERE IS A TAB MANAGER ON THE BOTTOM LEFT (OR ALT + T)

## Creating Parts

### Maintenance
* **NAME YOUR PARTS (NOT JUST THE PART STUDIO)**
* **DIMENSION ALL (EVERYTHING MUST BE BLACK)**
* NEVER USE FIX (unless it's the very first part)
* DO NOT MAKE IDENTICAL COPIES OF PARTS WITHIN A PARTS STUDIO
* Organize features in folders (for complicated studios)
* Delete all useless surfaces and curves
* Use CONFIGURATIONS for similar versions of the same part

### Importing
* Add all to the same imported parts studio

###  Modeling Guidelines
* Put geometrically related parts in the same studio
    - Yes, you can put multiple parts in a single studiosolidworks bad
* Make everything parametric
    - Create Variables for important dimensions (e.g. tube size)
    - Allow for people to mess with dimensions higher up in the feature tree without breaking the model
        - In short, design while keeping it easy to modify dimensions
* Use as few features as possible
* Keep Sketches simple
    - Try making all patterns as features, not within a sketch
    - Try making all fillets/chamfers 
    - Separate complicated sketches into multiple sketches
        - E.g one for construction lines and another for solid lines
* Make all holes using the Hole Feature
* If your Part Studio/Assembly contains multiple parts, make each unique part a different color

### Modifying Parts
* Modify an existing sketch/feature if possible, limit the number of additional sketches you use
* If there are any errors, please resolve them

### Assemblies
* IF A MODIFICATION BREAKS AN ASSEMBLY, PLEASE FIX EITHER THE MODIFICATION OR THE ASSEMBLY
* Fix ONLY the first instance
* USE GROUP AS MUCH AS POSSIBLE
* Try using more meaningful subassemblies (for organization)
* Use Standard Content to place screws and nuts
    - Mostly to check for interference
* Use Interference Analysis to check for interference 
* DO NOT USE EDIT IN CONTEXT AND REFER TO OTHER OBJECTS IN ASSEMBLIES UNLESS 
* SHOW ALL INSTANCES ONCE YOU ARE DONE
* HIDE ALL MATES AND MATE CONNECTORS
* MAKE BOMs (Bill of Materials) FOR STUFF

### Drawings
Please abide by [PLTW dimensioning standards](https://studylib.net/doc/25279793/pltw-7.1.a-dimensioning-standards)

