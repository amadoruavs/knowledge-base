# Git

Git is a powerful version tracking and collaboration tool for maintaining
codebases.

https://docs.google.com/document/d/1wXhE6iufxvKbflmhg9mw6kOAS1zfcqIX68qxf3-ZGt4/edit?usp=sharing

See the Handbook for a guide on using Git.
