# MicroROS

MicroROS is a ROS2 variant that runs on microcontrollers.
It provides the same powerful IPC API that ROS2 does, and
integrates with ROS2 stacks on regular OSes.

Pros:
- Integration with ROS2 stacks on actual computers
- Great IPC support with pub/sub and service/client models
- Same familiar ROS2 API (rclcpp, plus a C library, rclc)
Cons:
- Pretty heavy (sample ping pong application is 152k)

## Getting Started

MicroROS can run on top of many popular RTOSes, such as NuttX and Zephyr.
We recommend running MicroROS on top of Zephyr.

The official guide is [here](https://micro-ros.github.io//docs/tutorials/core/first_application_rtos/zephyr/). This guide details things not included in the official guide that you may want to know.

While the official docs target the [Olimex STM32-E407 board](https://www.olimex.com/Products/ARM/ST/STM32-E407/open-source-hardware), our guide targets the much simpler and cheaper STM32F401CE (Black Pill) microcontroller. However, MicroROS can be deployed on
any board supported by Zephyr that has the resources to run it.

MicroROS is fairly heavy; we recommend a board with 256K flash or more.

### Required Materials
You will need:
- A computer with an OS capable of running ROS (we recommend Ubuntu 20.04)
- An STM32 Black Pill or similar board
- A programmer (this guide uses the ST-Link, but other programmers should work)
- A USB-C cable

### Step 0: Hardware
Plug the USB-C cable into the USB port on the board.
Plug in the programmer as well. We're assuming you know how to plug pins
into other pins.

### Step 1: ROS2 setup

Follow [the official guide](https://micro-ros.github.io//docs/tutorials/core/first_application_rtos/zephyr/) to set up the ROS2 workspace. To build for the blackpill,
change `olimex-stm32-e407` to `blackpill_f401ce` (or any other zephyr board description).

The official guide builds a sample application from the firmware/zephyr_apps example
directory. You may create your own packages in `src/`; just remember to `colcon build`.

NOTE: Changes to workspace code will not take effect until you `colcon build`.

### Step 2: Configure, Build and Flash

Again, [follow the official guide](https://micro-ros.github.io//docs/tutorials/core/first_application_rtos/zephyr/).

Note that the guide chooses `serial-usb` as the transport layer. This means
ROS2 will communicate over USB (which is why we have a USB cable). `serial`
and `host-udp` transports are also available, although we will not cover
them.

We had difficulties with the included MicroROS flash script, as `west flash`
attempted to force the board to flash in DFU mode, which we did not want.
You can flash the `firmware/build/zephyr/zephyr.bin` firmware file manually
with the tool of your choice; for the blackpill it's `st-flash write zephyr.bin 0x08000000`.

### Step 3: Profit

[Follow the official guide](https://micro-ros.github.io//docs/tutorials/core/first_application_rtos/zephyr/) to set up the microROS agent. This agent will allow the ROS2
stack running on your regular computer to connect to MicroROS, running on the
microcontroller.

Make sure the USB cable is plugged in, as we configured microROS to communicate
over it earlier. The serial device should show up under /dev/serial/by-id/{some microROS name}. Run the agent with `ros2 run micro_ros_agent micro_ros_agent serial --dev /dev/serial/by-id/{that microROS name}`.

Open another terminal, source the setup scripts, and run `ros2 topic echo /microROS/ping` (this is the default publish route in the sample ping pong application; in the real world, your microROS app can publish to any route it wants). It should begin outputting data regularly.

Congratulations! You have just gotten MicroROS working.

## Some Further Notes
MicroROS is a potentially very useful tool, especially for more complex
projects. ROS2's powerful node IPC allows the creation of modular and scalable
embedded applications. However, for smaller projects, its size may be an issue.

The regular [rclcpp API](https://github.com/ros2/rclcpp), as well as a minimal
[C api](https://github.com/ros2/rclc), are available to devs. Similarly,
[Zephyr](https://zephyrproject.org/) APIs should also be fully available.

Zephyr's `prj.conf` is represented in MicroROS
as specific to each transport type; e.g. `serial-usb.conf` will
be used as `prj.conf` if the serial-usb transport is used. Device trees
*should* work as usual, although we did not fully test them.

Take a look at the [sample MicroROS Zephyr Apps](https://github.com/micro-ROS/zephyr_apps/tree/foxy/apps) for more examples.

Also look at [the rclc guide](https://micro-ros.github.io/docs/tutorials/core/programming_rcl_rclc/).
