# Current Software Stack

This guide describes the current AUVSI-SUAS competition stack for
AmadorUAVs.

The [current iteration can be accessed on Lucidchart](https://lucid.app/lucidchart/3c8821d2-26cb-4deb-930e-01d214ac6056/edit?viewport_loc=-539%2C-238%2C4122%2C2278%2Ch4~wGz6gf2Ld&invitationId=inv_76187db3-45df-440f-ae26-52388c929136).

![Software Stack - Current Iteration](../images/gridgen.png)

## Interop

See the [main article](general/interop.md).

## Ground Control Station

The Ground Control Station (GCS) is the central control station for the drone.
It reads the drone's telemetry (information), and can send commands to the drone.

Our current Ground Control Station is [QGroundControl](http://qgroundcontrol.com/).

## Macron

Macron is our main competition drone. It communicates with the GCS and other
software.

## Pilot

Pilot is our main path planning system. It reads information from the Interop
server, and generates .plan files read by the GCS. The GCS then sends the mission
plans to the drone.

## Azimuth

Azimuth is our
