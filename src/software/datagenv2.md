# Generating training data with Datagenv2

- Uses Python's PIL library
- Generates images and annotations for locations of ODLCs
- Makes use of local files for generation of every shape
- Running start.py initiates 5 parallel runs of generator.py; this dramatically reduces generation time
- Annotations are currently in YOLOv5 format (https://roboflow.com/formats/yolo-darknet-txt)
- generate class: selects shape (random or provided as an argument), color, character (random or provided as an argument), position, rotation, size, etc.
- main() function: calls generate class multiple times to generate shapes; number of shapes, file location of written shapes/annotations, & number of images are decided in this function
