# Software Divison Guided Dive

This guide is intended to give you all the information you need
in order to become a full-fledged member of the AmadorUAVs Software
Team.

Obviously, there's a lot of information here and you're not *required*
to read all of it. Here's how we recommend you read the guide:

- ***All members should read the general information section.*** It's pretty
important, because otherwise when we talk about ODLCs or Interoperator or
whatever, you'll have no idea what we're talking about.

- Members specializing in a field (computer vision, path planning etc.) are
recommended to read the sections under their field.

- Members planning to run for a leadership position should read all sections
under this guide.


While most information should have been migrated here, you can also
check the [original Software Handbook](https://docs.google.com/document/d/1wXhE6iufxvKbflmhg9mw6kOAS1zfcqIX68qxf3-ZGt4/edit?usp=sharing) for information.
