# Interop Server

The Interop server is the central competition server which holds mission descriptions
and presents endpoints for teams to upload telemetry and ODLCs.

The main Interop documentation can be found [here.](https://github.com/auvsi-suas/interop)

## Running a Server

Documentation for running a server can be found on the main README. Essentially,
you need to:
1. Install Docker and Docker-Compose
2. Clone the repository
3. Run the docker-compose.yml configuration in the server folder

This should make an accessible Interop server on `localhost:8000`. This can be
verified by visiting the URL in your web browser.

## API Usage

Our team's main method of interfacing with Interop is with our Interoperator
library. See the documentation in the left sidebar.

Essentially all API endpoints require a login. See main documentation for info.

## Mission Descriptions

The Interop server contains all descriptions of competition flight profiles,
including waypoints, area survey boundaries, geofence boundaries, and more.
Each mission description is accompanied by an ID that will be given at comp.

The interoperator library can be used to access these descriptions.

All ODLCs must be associated with a mission ID.

### Creating a Mission Description

The easiest method of creating a mission description is via the `qgc2interop`
utility our team maintains. To use this utility:
0. Make sure you have cloned and set up the interop server first. Additionally,
make sure it is not running.
1. Open QGroundControl and create a new mission. Add waypoints and Area Survey
items.
2. Obstacles can be added by creating circular exclusion geofence items. Geofences
can be created by creating polygonal inclusion geofence itms.
3. All other points will be created at the home position. (TODO: Improve)
4. Once you have created your mission, save it as a .plan file.
5. `./convert.sh [planfile] [path_to_interop_server]`

See the instructions on the README for more details.

## ODLCs

ODLCs stand for Object Localization, Detection, Classification; they are the
main object detection task at comp.

There are 2 types of ODLCs; standard and emergent. Standard ODLCs have:
- A shape background
- A letter or number on the shape

Attributes we need to classify are:
- Shape
- Letter
- Shape Color
- Letter Color
- Orientation

See IMS for approaches. The Interoperator library can be used to upload ODLCs.
