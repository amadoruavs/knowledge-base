# PWM: Analog vs Digital/RC/Servo

There are generally two types of PWM relevant in the field of robotics. They use the same underlying concept (pulse width modulation) but differ in other ways. This causes a lot of confusion, which this article attempts to clarify.

## Recap: PWM
As a reminder, PWM (pulse width modulation) is a type of rectangular waveform where a chip (like a microcontroller) quickly changes a digital line between logical LOW and HIGH voltage at a specified frequency. Width modulation refers to the technique of varying the amount of time spent in the HIGH state in a cycle (as well as the frequency).

## Analog PWM
The first use of PWM is to approximate an analog signal. When you're using a digital IO line (from a microcontroller), and don't have a DAC, sometimes you want to be able to create a psuedo-analog value. An example would be to control the brightness of an LED. You can do this by quickly switching the line between HIGH and LOW. The percentage of time in a single PWM cycle spent in HIGH (t_high / period) is known as the *duty cycle* of the PWM. When the PWM is carried out at a high enough frequency, the output is effectively driven at the same voltage as the duty cycle times the full IO voltage. The frequency required varies significantly - while a few hundred Hz is plenty to approximate an RGB LED as far as the human eye is concerned, accurately controlling a brushless motor in FOC may require frequencies in the tens of hundreds of kHz. Analog PWM is also used when using a transistor which can only be on or off, to drive the output at a varied voltage level.

PWM is usually accomplished by configuring a hardware timer peripheral on an MCU to directly drive a digital IO line, although you're welcome to bit-bang (although wouldn't recommend it for performance reasons) or use an external PWM generation chip.

Don't try to control a servo or ESC using this. It won't work at all.

When using the Arduino library, `analogWrite()` uses this (unless you have a DAC, which is only available on a few ARM chips).

## Digital PWM
At some point, someone realized that PWM can also be used as a simple digital-ish communication protocol. This is commonly used in RC servos and ESCs. It is the type of PWM signal outputted by RC controllers and the servo/ESC rail on a flight controller.

Recall that in analog PWM, the frequency is irrelevant (usually kept as high as reasonable) to create the illusion of an analog signal. The duty cycle is then used to change the pseudo-analog voltage. Digital PWM DOES NOT TRY TO APPROXIMATE AN ANALOG SIGNAL. It instead uses a fixed frequency of usually either 50Hz or 400Hz. The duty cycle is not treated as a percentage; instead, t_high ranges from 1000us - 2000us. This represents a digital value from 0 to 100% something (throttle, servo arm deflection, amount of water in a bottle, doesn't really matter). It is the receiver's problem to measure the time between a rising edge and a falling edge and map that back into a value. A new data sample can be sent every PWM cycle.

This is a "digital" protocol. It cannot send any data other than a single number in the range [1000, 2000] 50-400 times a second, and doesn't send any bytes over the wire directly. However, it is still carrying a digital value encoded in the pulse width. It is NOT an analog protocol. Don't try to plug in an LED and vary its brightness with this method. The LED will change brightness levels, but not in the way you expect.

When using the Arduino library, the `Servo` library/package can be used for this.
