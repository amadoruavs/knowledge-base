# Publish/Subscribe

When you're developing for embedded and you're using a multitasking environment (RTOS i.e. Zephyr), it's often necessary to pass data between threads. A very simple Zephyr library has been created for this purpose: [coderkalyan/pubsub](https://github.com/coderkalyan/pubsub). Usage docs can be found on the project README. The library can also be ported to platforms other than Zephyr (although has not been at the time of writing).
