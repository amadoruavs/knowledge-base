# Interoperator

Interoperator is our club's own Interop Python library.
It wraps the various API endpoints.

Currently there are two versions located within our Gitlab:

- [Interoperator v1](https://gitlab.com/amadoruavs/interoperator) is the asyncio/AIOHTTP version of Interoperator.
It has since been deprecated in favor of v2 due to the complex async API.
- [Interoperator v2](https://gitlab.com/amadoruavs/interoperator-v2/) is the current version of Interoperator, based on
the requests library. It has yet to be integrated with services.

All documentation can be found within the repositories' READMEs or in their
`__main__.py` files.
