# Azimuth / Antenna Tracker Control Architecture

The antenna tracker is currently comprised of two parts:
- The high-level Azimuth guidance system
- The Klipper firmware, which controls the motors

This document goes over the current approach, and the issues to fix.

## The Current Approach

### Klipper

Klipper is an open-source 3D printer firmware, repurposed in the antenna tracker
as a general-purpose stepper motor controller. It is capable of receiving GCode commands
(CNC machine commands) to move the stepper with a certain velocity and acceleration.

However, due to its GCode processing nature, Klipper can only move motors in discrete steps -
one command must complete for the next to begin. This is a major disadvantage because the antenna
tracker can only move in discrete steps instead of constantly tracking towards the drone.

### Azimuth

Azimuth is comprised of the central angle estimation code, the MAVLink module, the antenna tracker
position module, and the Klipper interfacing module.

The central angle estimation code uses an ECEF (Earth Center Earth Fixed) projection to estimate the target angle
given the GPS position and altitude of the drone and antenna tracker (for more info on this topic, search for
"telescope tracking algorithms").

The MAVLink module utilizes PyMAVLink to connect to the drone, reading GLOBAL_POSITION_INT messages from the
drone's system ID to update the drone's current GPS position in a constantly running thread. 
These GPS positions are fed into the angle estimation code.

The Klipper interfacing module handles sending control commands to Klipper. It uses MANUAL_STEPPER commands to
feed the angle to the azimuth and elevation motors. However, it can only update
the target angle a few times per second, since Klipper queues commands. It is highly recommended that this
module be replaced.

The antenna tracker position module estimates the current position of the antenna tracker. Currently, the position
module is simply a webserver which is contacted by an iOS app with the GPS coordinate. However, this approach 
is highly inaccurate and led to the failure of the antenna tracker at the 2022 competition. It is highly recommended
that this module be replaced - either by reading the RTK GPS base position from QGroundControl using PyMAVLink, or
utilizing a separate GPS module.