# PID

PID stands for Proportional-Integral-Derivative. It is extremely common in control theory and is used
to control an output (e.g. motor speed) based on an input (e.g. angle).

## Error

A PID controller tries to do one thing - move something from where it currently is, to where you want
it to be. For example, a drone has uses a PID loop to level itself. It tries to adjust itself from
whatever angle it is currently at, to being at 0 degrees - flat.


To make things clear, we'll look at a simple example in your home - the thermostat.
- In this example, the current temperature of your house is the *current state*; where it is right now.
- In this example, the thermostat being set to 75 degrees F(reedom) is the *target state*; where it wants to be.

The difference between *current state* and *target state* is the *error*.

## A simple controller

What's the simplest way to make the house heat up? Well, we can turn the heater on when the temperature
is below the set temperature, and turn it off when it reaches the set temperature. We call this approach
an on-off controller. This is a very easy approach to use, and in fact it works well in many cases -
most thermostats *are* on-off controllers, because it's simple and cheap.

On-off controllers come with several issues, though. One major one is oscillation - since just turning the heater
on and off isn't very precise, your house will fluctuate between being slightly too hot and slightly
too cold. While it's not a big deal in your house, it is a big deal to something like a drone trying to
stay level in the air.

## Proportional Controlling

Let's try making our thermostat a bit smoother. Instead of having our heater 100% on or completely off, we
can make our heater power *proportional* to the difference between the house temperature and the thermostat
temperature. This way, as the house temperature gets closer to the thermostat temperature, we can reduce
power to the heater, giving us a smooth ramp. If the house temperature is way below the thermostat temperature,
we can give more power to the heater to heat up faster.

This is called a *proportional* controller because it's proportional to the *error* (in this case, the difference
between the thermostat temperature and the house temperature). Already this controller is much smoother than
the on-off controller.

We can control how fast the proportional controller ramps up by using a *constant* (we'll call it Kp) to multiply the error. If we 
increase Kp, the controller will ramp up faster (Kp * e).

### What are our issues?

While proportional controllers are much smoother than on-off controllers, if you set Kp too high, it'll overshoot the target and
oscillate similar to the on-off controller.

Furthermore, if something else changes in the environment (like you leaving your freezer open by accident, thus lowering your
house temperature), then the proportional controller won't change to overcome it, instead putting out the same heater power.
This can lead to the output getting "stuck" before reaching the target.

We can solve this issue by adding *integral* and *derivative* terms.