# Grid Generation & Avoidance

## Grid Generation
Grid generation is our current programmatic approach to solving the majority of competition tasks. 

Grid generation creates a bounding rectangle around the flight boundary polygon and uses this to essentially create a 2D representation of the flight area. Obviously there are an infinitesimal amount of points inside the flight area, so it is not possible to calculate and plot every single point, but we can get very close by reducing the distance between each adjacent point to a certain set grid accuracy. This set grid accuracy distance is then used to calculate points in each row and column of the bounding rectangle. The [equirectangular projection](https://en.wikipedia.org/wiki/Equirectangular_projection)  is used to transform GPS points into cartesian points and vice versa, allowing the adjacent points in the grid to be calculated easily by simply adding some distance to a cartesian point and transforming the point back to GPS. The equirectangular projection is quite useful as it allows us to treat the points inside the grid as cartesian points, without sacrificing significant precision in the actual GPS points.

However this approach does have some drawbacks–creating a grid implies an O(n<sup>2</sup>) complexity, so reducing grid accuracy can quickly lead to long runtimes. Right now grid generation (C++ module) can comfortably support 0.5 m of grid accuracy with a runtime of ~2 seconds.

![grid](../../images/gridgen.png)

## Obstacle Avoidance
Our current approach to obstacle avoidance between points relies on using the generated bounding grid to perform the [A* path searching algorithm](https://en.wikipedia.org/wiki/A*_search_algorithm).

The first step to avoidance consists of plotting all obstacles of the mission on the bounding grid. Due to the rectangular nature of the bounding grid, plotting a circle on the grid would result in the edges of the circle being uneven on the grid, therefore giving paths with many zig-zags around the obstacle. Instead we opted to bound the circle obstacle with an octagon, which both minimizes the extra space we add to each obstacle while having a very linear shape, which  produces very linear paths when avoided. 

![octagon](../../images/octagon.png)

After the obstacles are plotted, obstacle avoidance is performed by finding a point A where we want to start from and a point B where we want to reach, and simply running A* on both points. 

The last step consists of compressing the outputted A* path by removing unnecessary intermediate points, as we want to minimize as many points as possible when creating a planfile.

![avoidance](../../images/avoidance.png)
 
