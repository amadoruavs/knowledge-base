# IMS

IMS, short for Image Management System, is what handles the entire imaging/vision side of the competition.
It consists of three main parts:
- The IMS frontend, built in Vue.JS using the Konva canvas system
- The IMS backend, built using the Django Python framework
- Photographer, the drone-side capture module

[Photographer docs are hosted here.](/src/software/photographer/README.md) This article
concerns mainly the IMS backend (as the frontend is fairly standard for Vue applications)
and its architecture.

## The IMS Backend

The IMS backend is built as a traditional HTTP REST API server, built on the Django framework.
It heavily utilizes the Django REST framework to automate the creation of APIs.

Before proceeding, familiarize yourself with:
- [REST APIs](https://www.redhat.com/en/topics/api/what-is-a-rest-api)
- [Django](https://docs.djangoproject.com/en/4.0/intro/overview/)
- [Django REST Framework](https://www.django-rest-framework.org/)

## Models
IMS data is represented in the database as relational *models*. These models, are as follows:
- Mission, which represents a competition Interop mission
- Image, which contains the full image data and metadata such as the longitude, latitude and height
of the drone when it was taken; linked to Mission
- ODLCs, which represent ODLCs found in the image; linked to an Image

## Async Tasks
IMS runs CV models in the background when images are saved in order to find and classify ODLCs.
This is intended to assist manual classification, with an eventual goal of fully automatic
classification.

IMS currently uses the Django-Q task system to execute asynchronous tasks such as CV models.
However, Django-Q does not have initialization control (a hacky workaround is to run images
through the backend until all threads are initialized), so it takes time for models to warm up.

It is recommended that IMS switch to the Celery task system, which offers initialization control
and is more widespread, as soon as possible.

Please check [interop.py](https://gitlab.com/amadoruavs/ims/classitron/-/blob/master/classitron/classify/tasks/interop.py) for
and example of defining a task, and [views.py](https://gitlab.com/amadoruavs/ims/classitron/-/blob/master/classitron/classify/views.py) (`upload_to_interop` method) for an example of how to trigger an async task.

Further Django-Q documentation can be found [here](https://django-q.readthedocs.io/en/latest/).
Celery docs can be found [here](https://docs.celeryq.dev/en/stable/index.html).

## Data Flow

When Photographer is started, a Mission item is initialized. 
When Photographer takes an image, it is sent to the IMS image API.

The creation of an Image object triggers an async classification task, which starts locating and
classifying ODLCs. A task ID is (supposed to be, currently not) added to the object to keep track
of the status.

Any ODLCs classified will be created as ODLC model objects on the backend.

The frontend will receive the image with classified ODLCs. Any necessary edits will be made, and
the user will use the frontend to submit the ODLC. This submission triggers another asynchronous
task to upload the ODLC to the competition server, completing the data flow.

## Q
Questions should be directed towards Raghav Misra, current IMS maintainer.
Unanswerable questions may be directed to vwangsf@gmail.com.