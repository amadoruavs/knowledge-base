# Photographer

Photographer is our drone-side image capture and first-stage detection system.
It:
- Captures images from our onboard camera
- Finds ODLCs in the image, and sends it to IMS
- Can be controlled via a builtin Flask HTTP server API
    - Can switch between ODLC search mode and area survey mode
    - Can control the gimbal through requests
    - Can process image capture commands (given waypoint to capture at
      and correct order)

## Architecture

Photographer's repository structure is a *standard Python package structure*. See general software info for more details.

Photographer's architecture is relatively simple. It contains:
- A drone interfacing module, which uses MAVSDK to get telemetry
updates from the drone and send control commands
- A main image capture module
  - Image capture module in Survey mode handles sending images to ODM
- A YOLOv5 based detection module (PLANNED: ORB or blob-based detection)
  - Detection module handles sending ODLCs to IMS
- An HTTP server for control

Breaking it down file-by-file:

- `detect.py` contains the main YOLOv5 implementation for the region-of-interest detector. This module finds ODLCs in the image and crops them out for sending to ground.
- `drone.py` contains the main drone interfacing module. A MAVSDK connection
is run in a background module, which continuously monitors the drone's position and
other info.
- `image_capture.py` is the drone's main image capturing module. It handles grabbing
images from the drone and sending to ODM.
- `server.py` is the drone's builtin HTTP server. It currently contains a few
waypoints for controlling ODLC parameters, but will be expanded for mapping.

## Instructions to Run

Configure photographer to start on boot for the Jetson Nano. If not correctly
configured, set the IMS_API_URL, USER and PASSWORD in `settings.py`. Also
correctly set the drone MAVSDK address, the video device and other settings.

If running locally for simulation or testing, point the IMS server at your
local server and the drone address at the simulated drone.


## Current Todo List

- `detect.py` should likely be refactored to take image input from the
image capture module, rather than having its own image capture. We need to check
performance impacts of this.
- `drone.py` should be updated with mission item progress, control/hold abilities
and the ability to notify when a position has been reached.
- Gimbal control still needs to be written.
- Mapping API for HTTP server needs to be written.
- `image_capture.py` still needs to implement the survey mode. Additionally, an
ODM-side server must be made to handle uploaded images, or this functionality
should be integrated into IMS.
- Simulated video output should be integrated into `image_capture.py`.
