# GStreamer on SRT: Bugfixes

SRT (Secure Reliable Transport) is a streaming protocol. GStreamer supports it with the `srtsrc` and `srtsink` plugins.

SRT is included in its current form in GStreamer 1.16+. GStreamer 1.14+ has them as `srt{server, client}{src, sink}`.

## GStreamer on NVIDIA Jetson Nano (Ubuntu 18.04)

The default GStreamer version included on the Jetson Nano OS is 1.14.
The old SRT API *should* work, but we recommend upgrading to 1.16 to use the newer
unified SRT API.

To obtain GStreamer 1.16 with SRT support:

1. Download, compile and install the [libsrt library](https://github.com/Haivision/srt). Build instructions are included. **Use the v1.4.1 tag.**
2. Use NVIDIA's included `gst-install` script to install GStreamer 1.16 with SRT + nvidia hardware accel:
   `sudo gst-install --prefix=$HOME/gst-1.16.2 --version=1.16.2`
3. But wait! Pause it after it downloads the GStreamer plugin sources.
4. Open `/tmp/gstreamer-build/gst-plugins-bad-1.16.2/ext/srt/gstsrtobject.c`, go to line 130, and remove the line containing `SRTO_LINGER`.
5. Re-run the `gst-install` command above. It should use the existing sources to build.
6. Wait for it to finish.
7. Go into the installed GStreamer and remove the libgstopengl plugin files (`rm $HOME/gst-1.16.2/lib/aarch64-linux-gnu/gstreamer-1.0/libgstopengl*`).
   These files do not appear to load properly, and will stall gstreamer; remove them.
8. GStreamer should be ready for use. Use `gst-launch-1.0 --version` to see that it has installed properly.

NOTE: Because NVIDIA at the time of writing has not updated the Jetson to Ubuntu 20.04, GStreamer 1.16 is the best you will get (1.18 requires dependencies
newer than the ones in the package repos). While you *might* be able to compile 1.18, it's really not worth it.
