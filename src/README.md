<div id="frontmatter" style="display: none">
title: "yourmom"
</div>

# KnowledgeBase

Welcome. You've found the AmadorUAVs Knowledge Base.

This knowledge base is an ongoing project to document all knowledge the AmadorUAVs
team has accrued over the years, including knowledge of how to maintain the software
stack and its tools, how to debug common PX4/QGC/MAVLink issues, and more.

Each topic will have a relevant .md Markdown file. You can use the Gitlab file browser
to browse (or, if you prefer, clone the project and read them with your editor of
choice).

Adding to this is the same as adding to any other UAVs repo,

We've made this a git repo under the software team because it's less likely to get lost this way, compared to some floating Google Doc.

Visit the Gitlab repo to add or edit the content of these docs: [https://gitlab.com/amadoruavs/knowledge-base](https://gitlab.com/amadoruavs/knowledge-base).

<script>
  if (window.netlifyIdentity) {
    window.netlifyIdentity.on("init", user => {
      if (!user) {
        window.netlifyIdentity.on("login", () => {
          document.location.href = "/admin/";
        });
      }
    });
  }
</script>
